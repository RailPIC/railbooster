/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "user.h"

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* Baseline devices don't have interrupts. Unfortunately the baseline detection 
 * macro is named _PIC12 */

#ifndef _PIC12

void interrupt isr(void) {
    /* This code stub shows general interrupt handling.  Note that these
    conditional statements are not handled within 3 seperate if blocks.
    Do not use a seperate if block for each interrupt flag to avoid run
    time errors. */

#if 0

    /* TODO Add interrupt routine code here. */

    /* Determine which flag generated the interrupt */
    if (<Interrupt Flag 1 >) {
        <Interrupt Flag 1 = 0 >; /* Clear Interrupt Flag 1 */
    } else if (<Interrupt Flag 2 >) {
        <Interrupt Flag 2 = 0 >; /* Clear Interrupt Flag 2 */
    } else {
        /* Unhandled interrupts */
    }

#endif

    if (GPIF) {
        startTimeout();
        if (shortNOTdetected) {
            if (IN == 1) {
                GPIO = GPIO_OUT1; //to make change in same signal 
            } else {
                GPIO = GPIO_OUT2;
            }
        }
        GPIF = 0;
        if (TMR1 > TMR1_1ms) {
            requestMeasure = 1; //Measrue every 1 ms if there is activity (enable=1) and we receive signal.
            startTimeoutMeasure();
/*
            if (!shortNOTdetected){
                ENABLE=~ENABLE;
            }
 */
        }
    } else if (TMR1IF) { //2 ms no change os signal (enable on or not))
        requestMeasure = 2; //Measrue every 2 ms with enable, measure consumition, withou.. measure limit.
        startTimeoutMeasure();
        TMR1IF = 0;
    } else { //timeoout
        ENABLE = 0;
        //TMR1ON=0;
        T0IF = 0;
    }
}
#endif
