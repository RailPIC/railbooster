/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */

/******************************************************************************/

void main(void) {
    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    InitApp();
    /*
    //TEST AD
        GIE=0; 
        ENABLE=1;
        __delay_ms(1000);
         ENABLE=0;
          __delay_ms(1000);
          ENABLE=1;
          GIE=1; 
     */

    while (1) {
        if (requestMeasure > 0) {
            if (ENABLE == 1) {
                //mide consumo casa 1 o 2 ms... if enable no short in process.. we measure consumtion always
                measureConsumtion();
            } else {
                //it could be a short inprocess or not.. if not.. just measure limit.. incase we want to adjjust... if short in process... we measure consumtion to see if we can release the short condition and return enable
                //GP2=~GP2;
                measureLimit();
                if (!(shortNOTdetected)) {
                    measureConsumtion();  
                }
            }
            requestMeasure = 0;
        }
    }
}

