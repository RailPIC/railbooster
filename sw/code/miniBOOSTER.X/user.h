/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

//#define TRISIOCOnf 0b00101101 //GP0, GP1 GP4 output.. the rest input    
#define TRISIOCOnf 0b00011001 //GP0, GP1 GP4 output.. the rest input    
/*	GP0.Consumo (AN0)
	GP1 out1
	GP2 out2
	GP3 in      
	GP4 Limit Potenciometro (AN3)
	GP5 enable
*/

#define IN          GP3
#define ENABLE      GP5
#define GPIO_OUT1   0b00100010   //GP5 enable, OUT2 OFF (GP2) OUT1 ON (GP1)
#define GPIO_OUT2   0b00100100   //GP5 enable, OUT1 ON (GP2) OUT1 OFF (GP1)
#define CMCONConf 0b00000111 //Comparator off

//#define ANSELConf 0b00011001      //Fosc<8 (a 4Mhez, Tad=2us). AN3 y An0 Analogo
#define ANSELConf 0b01011001      //Fosc/16 (a 4Mhez, Tad=4us). AN3 y An0 Analogo
#define ADCON0Conf  0b10000000     //Conversion right justified Vdd as Vref
#define ADCON0AN0on 0b10000001     //Conversion right justified
#define ADCON0AN3on 0b10001101     //Conversion right justified

#define OPTION_REGConf  0b10000111  //256 prescaler for timer0
//#define TMR0_30ms  138  //fosc=4Mhz Tins=1/fosc*4=1us;Tins*prescaler(256)=256us 3ms=117,18*256us.
//Uso 118; contamos hacia delante: 256-118; inicio contador 138

#define TMR0_30ms  0  //Test,,, tiu�ineout est�a cirtando se�ales

#define T1CONConf   0b00110000  //TIMER1 off, prescaler 8
#define TMR1_2ms  65286 //fosc=4Mhz Tins=1/fosc*4=1us;Tins*prescaler(8)=8us 2ms=250*8us
//Overflow 65536-250=; inicio contrador en 65286 cuenta 250 y overflow  //TMR1 16 bits!!!
#define TMR1_1ms  65411 //1ms=125*8us
//125 is not a countdown.. but 125 from the start of the clock 65536-125=131

#define INTCONConf   0b11001000      //Interrupts avaialble Peripherial available timer0 OFF, and  change GPIO (for GPIO3)
//#define INTCONConf   0b01001000      //Interrupts NOT avaialble Peripherial available timer0 OFF, and  change GPIO (for GPIO3)
//#define INTCONTMR0on 0b11101000      //Interrupts avaialble Peripherial available timer0, and  change GPIO (for GPIO3)
#define IOCConf      0b00001000         //Interrupt on change GP3
#define PIE1Conf     0b00000001     //TMR1 interrupt 

#define shortRETURN 5000             //5000ms to try to recover from short.. for some reason it goes to 8-9s... (resolved with Consumtion Limit inverse table)
//#define shortRETURN 1000             //Ver archivo analisis..la multiplicacion de shortlimit retrasa enmucho todo el proceso de vuelta a la normalidad
#define shortALERT  20              //20 ms with short cut off
//#define KClimit     0.07            //K to calculate the maximum Xonsumtion depending on potentiometer
/*
 * ImaxA=ImaxD*3,5/1023              //ImaxA in analogic value; ImaxD from conversor AD; 3,5 the maximum value 3,5 A
 * 3,5 A=1023
 * 1 A= 292
 * 
 * Climit=Vrsense*1023/Vref         //Vref=5V; Vresen=Rsense*ImaxA; Climit=Consumition limit
 * Climit=Rsense*ImaxA*1023/5=Rsense*ImaxD*3,5*1023/1023*5=Rsense*ImaxD*3,5/5
 * Rsense=0'1 in my desing
 * Climit=ImaxD*0,1*3,5/5=0,07*ImaxD
 * Check Spreadsheet for otrer values and jpeg for formulas
 
 */


//coming from manual and http://www.microchip.com/forums/m701812-print.aspx

#define testbit(var, bit)   ((var) & (1 <<(bit)))
#define setbit(var, bit)    ((var) |= (1 << (bit)))
#define clrbit(var, bit)    ((var) &= ~(1 << (bit)))
#define bittgl(var,bit)     ((var) ^= (1<<(bit)))

#define byte_of(your_var,byte_num) (*(((unsigned char*)&your_var)+byte_num))


/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/
extern bool shortNOTdetected;
extern unsigned char requestMeasure;
extern unsigned int  consumtionLimit;
//extern unsigned char shortThreat;
extern unsigned int shortThreat;

void InitApp(void);         /* I/O and Peripheral Initialization */
void startTimeout(void);
void startTimeoutMeasure(void);
void measureConsumtion(void);
void measureLimit(void);
//void translateImaxD(int ImaxD);