/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "user.h"
#include "system.h"        /* System funct/params, like osc/peripheral config */

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */

bool shortNOTdetected = true;
unsigned char requestMeasure = 0;
unsigned int consumtionLimit = 0;
//unsigned char shortThreat=0;
unsigned int shortThreat = 0;

//const char CLIMIT[1024]={0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,12,12,12,12,12,12,12,12,12,12,12,12,12,12,13,13,13,13,13,13,13,13,13,13,13,13,13,13,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,15,15,15,15,15,15,15,15,15,15,15,15,15,16,16,16,16,16,16,16,16,16,16,16,16,16,16,17,17,17,17,17,17,17,17,17,17,17,17,17,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,19,19,19,19,19,19,19,19,19,19,19,19,19,20,20,20,20,20,20,20,20,20,20,20,20,20,20,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,22,22,22,22,22,22,22,22,22,22,22,22,22,22,23,23,23,23,23,23,23,23,23,23,23,23,23,23,24,24,24,24,24,24,24,24,24,24,24,24,24,24,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,26,26,26,26,26,26,26,26,26,26,26,26,26,26,27,27,27,27,27,27,27,27,27,27,27,27,27,27,28,28,28,28,28,28,28,28,28,28,28,28,28,28,28,29,29,29,29,29,29,29,29,29,29,29,29,29,29,30,30,30,30,30,30,30,30,30,30,30,30,30,30,31,31,31,31,31,31,31,31,31,31,31,31,31,31,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,33,33,33,33,33,33,33,33,33,33,33,33,33,33,34,34,34,34,34,34,34,34,34,34,34,34,34,34,35,35,35,35,35,35,35,35,35,35,35,35,35,35,35,36,36,36,36,36,36,36,36,36,36,36,36,36,36,37,37,37,37,37,37,37,37,37,37,37,37,37,37,38,38,38,38,38,38,38,38,38,38,38,38,38,38,39,39,39,39,39,39,39,39,39,39,39,39,39,39,39,40,40,40,40,40,40,40,40,40,40,40,40,40,40,41,41,41,41,41,41,41,41,41,41,41,41,41,41,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,43,43,43,43,43,43,43,43,43,43,43,43,43,43,44,44,44,44,44,44,44,44,44,44,44,44,44,44,45,45,45,45,45,45,45,45,45,45,45,45,45,45,46,46,46,46,46,46,46,46,46,46,46,46,46,46,46,47,47,47,47,47,47,47,47,47,47,47,47,47,47,48,48,48,48,48,48,48,48,48,48,48,48,48,48,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,50,50,50,50,50,50,50,50,50,50,50,50,50,50,51,51,51,51,51,51,51,51,51,51,51,51,51,51,52,52,52,52,52,52,52,52,52,52,52,52,52,52,53,53,53,53,53,53,53,53,53,53,53,53,53,53,53,54,54,54,54,54,54,54,54,54,54,54,54,54,54,55,55,55,55,55,55,55,55,55,55,55,55,55,55,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,57,57,57,57,57,57,57,57,57,57,57,57,57,57,58,58,58,58,58,58,58,58,58,58,58,58,58,58,59,59,59,59,59,59,59,59,59,59,59,59,59,59,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,61,61,61,61,61,61,61,61,61,61,61,61,61,61,62,62,62,62,62,62,62,62,62,62,62,62,62,62,63,63,63,63,63,63,63,63,63,63,63,63,63,63,63,64,64,64,64,64,64,64,64,64,64,64,64,64,64,65,65,65,65,65,65,65,65,65,65,65,65,65,65,66,66,66,66,66,66,66,66,66,66,66,66,66,66,67,67,67,67,67,67,67,67,67,67,67,67,67,67,67,68,68,68,68,68,68,68,68,68,68,68,68,68,68,69,69,69,69,69,69,69,69,69,69,69,69,69,69,70,70,70,70,70,70,70,70,70,70,70,70,70,70,70,71,71,71,71,71,71,71,71,71,71,71,71,71,71,72,72};
//no room for the above table. Use the one below... just with the values of Imaxd that make Climit (the index of the array)  change. See spreasheet for more details
//const char CLIMITI[73]={0,7,21,35,49,64,78,92,107,121,135,149,164,178,192,207,221,235,249,264,278,292,307,321,335,349,364,378,392,407,421,435,449,464,478,492,507,521,535,549,564,578,592,607,621,635,649,664,678,692,707,721,735,749,764,778,792,807,821,835,849,864,878,892,907,921,935,949,964,978,992,1007,1021};
//Qith new schemam no need to transalte anything

void InitApp(void) {
    GIE = 0;
    //No interrupts yet... until measureLimit is ready... startTimeoutMeasure will start interrupts
    GPIO = 0;

    CMCON = CMCONConf;
    ANSEL = ANSELConf;
    ADCON0 = ADCON0Conf;
    T1CON = T1CONConf;
    TRISIO = TRISIOCOnf;

    OPTION_REG = OPTION_REGConf;
    PIE1 = PIE1Conf;

    //    INTCON=INTCONConf;

    IOC = IOCConf;

    __delay_ms(10);
    measureLimit();
    startTimeoutMeasure();
    INTCON = INTCONConf;

}

void startTimeout(void) {
    TMR0 = TMR0_30ms;
    T0IE = 1;
}

void startTimeoutMeasure(void) {
    TMR1ON = 0;
    //TMR1H=0;
    //TMR1L=TMR1_2ms;
    TMR1 = TMR1_2ms;
    TMR1ON = 1;
}

void measureLimit(void) {
    ADCON0 = ADCON0AN3on;
    GO_nDONE = 1;
    while (GO_nDONE) {
    } //Wait conversion is ready

    unsigned int ImaxD = ADRESL;
    byte_of(ImaxD, 1) = ADRESH;

  //  float tempCalc = KClimit*ImaxD;
   //consumtionLimit = (unsigned int) tempCalc;

   // consumtionLimit =CLIMIT[ImaxD];
   
    // translateImaxD(ImaxD);
    
    //With new schema; consumition limit= ImaxD
     consumtionLimit=ImaxD;
}

void measureConsumtion(void) {
    ADCON0 = ADCON0AN0on;
    GO_nDONE = 1;
    while (GO_nDONE) {
    } //Wait conversion is ready
    unsigned int consumtion = ADRESL;
    byte_of(consumtion, 1) = ADRESH;

    //Si consumo> limite y no hay corto.. que suba la amenaza de corto... si ya hay corto es que la amenaza est� al maximo.. no necesita subir m�s
    //if ((consumtion>consumtionLimit)&&(shortNOTdetected)){
    if ((consumtion > consumtionLimit)&&(shortThreat < shortALERT)) {
        shortThreat = shortThreat + requestMeasure;
        /*            eeprom_write(0x0A, shortThreat);
                    while(WR){}*/
        if ((shortThreat >= shortALERT)&&(shortNOTdetected)) { //need to check if we are not already in a short.. otherwise we keep increasing to sortMS
            shortNOTdetected = false;
            ENABLE = 0;
            shortThreat = shortRETURN; //We force... exiting short, requires some extra time
        }

    }        //Si conssumo<limite y aun no ha bajado amenaza a 0.. sigue banjando amenaza
        //Si amenaza ya ha llegado a cero que no baje m�s. 
        //Se baja la amenaza estando en estado de corto o no...
    else if ((consumtion < consumtionLimit) && (shortThreat > 1)) {
        shortThreat = shortThreat - requestMeasure;
        if (shortThreat < 2) { //1 or 0 is OK.. no more threat
            shortNOTdetected = true;
            shortThreat = 0; //shortThreat maybe already 1 or 0
            //TESTING LINE       
            // ENABLE=1; //enable will come if there is activity
        }
    }
}

/*
void translateImaxD(int ImaxD){

    bool found;
    char cindex;
    if (ImaxD==0){          //If no Id Max... cindex=0.. no current will be allowed
        found=true;
        cindex=0;
    }else{
        found=false;
        cindex=72;
    }
    while (!found){
       if (ImaxD>CLIMITI[cindex]){
           found=true;
       }else{
           cindex--;
       }
    }
    consumtionLimit =cindex;
}
 * 
 */