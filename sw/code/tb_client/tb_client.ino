#include "credentials.h"  //To include in a file of the details to connect to WiFi and TB
#define SERIALFEEDBACK      //Uncheck to have serial feedbcak
#define SERIAL_DEBUG_BAUD 115200U  //if not defined before
#define ATTRINUTE_ONE false  //Only send the attributes on the first connection. if there is telemetry.. telemetry will keep the device active.. otherwise...ATTRINUTE_ONE spould be false to keep sending attibutes and keep the device active

#include <EEPROM.h>
#define MEMO 0  //Wich EEPROM position start writing/reading
String token;

//In case of need to connect to Wifi 
//#include <ESP8266WiFi.h>
//#define WIFI_SSID "name_SSID"
//#define WIFI_PASSWORD "password_SSID"


#define THINGSBOARD_ENABLE_PROGMEM 0  //Needed by Thingboard.h.. otherwise it crashes
#include <ThingsBoard.h>
//arduino json v6.20.0... no va con 6.21.0
#define PROVISION_TIME 2000  //Time between processing 2000 ms
//In case of neednot provided in credentials.h 
//#define PROVISION_DEVICE_KEY "device_key"
//#define PROVISION_DEVICE_SECRET "device_secret"

String DEVICE_NAME;
bool sendATRIBUTES=true;

//Define KEYS for TELEMETRY
#define TEMPERATURE_KEY "temperature"
#define HUMIDITY_KEY "humidity"
//constexpr char TEMPERATURE_KEY[] = "temperature";
//constexpr char HUMIDITY_KEY[] = "humidity";

//In case not provided in crdentials.h
//#define THINGSBOARD_SERVER "TB-server"
#define THINGSBOARD_PORT 1883U
#define MAX_MESSAGE_SIZE 256U

//constexpr uint16_t THINGSBOARD_PORT = 1883U;
//constexpr uint32_t MAX_MESSAGE_SIZE = 256U;
#define CREDENTIALS_TYPE "credentialsType"
#define CREDENTIALS_VALUE "credentialsValue"
#define CLIENT_ID "clientId"
#define CLIENT_PASSWORD "password"
#define CLIENT_USERNAME "userName"
//constexpr char CREDENTIALS_TYPE[] = "credentialsType";
//constexpr char CREDENTIALS_VALUE[] = "credentialsValue";
//constexpr char CLIENT_ID[] = "clientId";
//constexpr char CLIENT_PASSWORD[] = "password";
//constexpr char CLIENT_USERNAME[] = "userName";

WiFiClient espClient;

// Initialize ThingsBoard instance with the maximum needed buffer size
ThingsBoard tb(espClient, MAX_MESSAGE_SIZE);

uint32_t previous_processing_time = 0U;

// Statuses for provisioning
bool provisionRequestSent = false;
bool provisionResponseProcessed = false;
//bool deviceProvisioned = false;

// Struct for client connecting after provisioning
struct Credentials {
  std::string client_id;
  std::string username;
  std::string password;
};
Credentials credentials;


/// @brief Initalizes WiFi connection,
// will endlessly delay until a connection has been successfully established

void InitWiFi() {

  Serial.println("Connecting to AP ...");
  // Attempting to establish a connection to the given WiFi network
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    // Delay 500ms until a connection has been succesfully established
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}

/// @brief Reconnects the WiFi uses InitWiFi if the connection has been removed
/// @return Returns true as soon as a connection has been established again
bool reconnect() {
  // Check to ensure we aren't connected yet
  const wl_status_t status = WiFi.status();
  if (status == WL_CONNECTED) {
    return true;
  }

  // If we aren't establish a new connection to the given WiFi network
  InitWiFi();
  return true;
}

void initTB() {
  //To be replaced with whatever is needed to get the name
  DEVICE_NAME="BO02";
  previous_processing_time = millis() - 2000;  //force first init
  if (readToken()) {
    provisionRequestSent = true;
    provisionResponseProcessed = true;
    //deviceProvisioned = true;
  } else {
#ifdef SERIALFEEDBACK
    Serial.println("No se encontró token de acceso, realizando self provisioning");
#endif
    provisionRequestSent = false;
    //provisionResponseProcessed=false;
    //deviceProvisioned=false;
  }
}

void telemetryTB() {
#ifdef SERIALFEEDBACK
  Serial.println("Sending telemetry...");
#endif

  //To be replaced with whatever is needed
  tb.sendTelemetryInt(TEMPERATURE_KEY, random(21));
  tb.sendTelemetryFloat(HUMIDITY_KEY, random(0, 2001) / 100.0);
}
void attributesTB() {
#ifdef SERIALFEEDBACK
  Serial.println("Sending attributes...");
#endif

  //To be replaced with whatever is needed
  const int attribute_items = 2;
  Attribute attributes[attribute_items] = {
    {"ip",  WiFi.localIP().toString().c_str() },
  //  { "active",       true     },
  //{ "ubicacion",       "ETRA"     },
  { "ubicacion",       "ETRA"     }
  };
  tb.sendAttributes(attributes, attribute_items);

#ifdef SERIALFEEDBACK
  Serial.print("IP: ");
  Serial.println(WiFi.localIP().toString().c_str());
#endif

}

bool readToken() {
  EEPROM.begin(512);
  for (int i = MEMO; i < EEPROM.length(); i++) {
    char c = EEPROM.read(i);
    if (c == '\0') {
      break;
    }
    token += c;
  }
#ifdef SERIALFEEDBACK
  Serial.print("Token: ");
  Serial.println(token);
#endif
  credentials.username = token.c_str();

  if (token == "") {
    return false;
  } else {
    return true;
  }
}

void loopTB() {
  if (millis() - previous_processing_time < PROVISION_TIME) {
    return;
  }
  previous_processing_time = millis();

  if (!provisionRequestSent) {
    if (!tb.connected()) {
      // Connect to the ThingsBoard server as a client wanting to provision a new device
#ifdef SERIALFEEDBACK
      Serial.printf("Provisioning. Connecting to: (%s)\n", THINGSBOARD_SERVER);
#endif
      yield();
      if (!tb.connect(THINGSBOARD_SERVER, "provision", THINGSBOARD_PORT)) {
#ifdef SERIALFEEDBACK
        Serial.println("Failed to connect");
#endif
        return;
      }
#ifdef SERIALFEEDBACK
      Serial.println("Sending provisioning request");
#endif
      // Send a claiming request without any device name (access token will be used as the device name)
      // if the string is empty or null, automatically checked by the sendProvisionRequest method
      const Provision_Callback provisionCallback(Access_Token(), &processProvisionResponse, PROVISION_DEVICE_KEY, PROVISION_DEVICE_SECRET, DEVICE_NAME.c_str());
      provisionRequestSent = tb.Provision_Request(provisionCallback);
      //attributesTB(); //Not proccessed yet the provision response and I want to send the attributes on first connection always.. not only on ptovisioning
    }
  } else if (provisionResponseProcessed) {
    if (!tb.connected()) {
      // Connect to the ThingsBoard server, as the provisioned client
#ifdef SERIALFEEDBACK
      Serial.printf("Connecting to: (%s)\n", THINGSBOARD_SERVER);
#endif
      if (!tb.connect(THINGSBOARD_SERVER, credentials.username.c_str(), THINGSBOARD_PORT, credentials.client_id.c_str(), credentials.password.c_str())) {
#ifdef SERIALFEEDBACK
        Serial.println("Failed to connect");
        Serial.println(credentials.client_id.c_str());
        Serial.println(credentials.username.c_str());
        Serial.println(credentials.password.c_str());
#endif
        //Try provisioning again
        provisionRequestSent = false;
        provisionResponseProcessed = false;
        return;
      } else {
#ifdef SERIALFEEDBACK
        Serial.println("Connected!");
#endif
      }
    } else {
      //To be changed with whatever telemetry to b sent
      telemetryTB();
      delay(500);
      if (sendATRIBUTES){
        attributesTB();
        if (ATTRINUTE_ONE) sendATRIBUTES=false;
      }
    }
  }
  //yield();
  tb.loop();
}

void setup() {
  // initialize serial for debugging
  Serial.begin(SERIAL_DEBUG_BAUD);
  delay(1000);
  InitWiFi();

  initTB();
}

void processProvisionResponse(const Provision_Data &data) {
  int jsonSize = JSON_STRING_SIZE(measureJson(data));
  char buffer[jsonSize];
  serializeJson(data, buffer, jsonSize);
#ifdef SERIALFEEDBACK
  Serial.printf("Received device provision response (%s)\n", buffer);
#endif
  if (strncmp(data["status"], "SUCCESS", strlen("SUCCESS")) != 0) {
#ifdef SERIALFEEDBACK
    Serial.printf("Provision response contains the error: (%s)\n", data["errorMsg"].as<const char *>());
#endif
    return;
  }

  if (strncmp(data[CREDENTIALS_TYPE], ACCESS_TOKEN_CRED_TYPE, strlen(ACCESS_TOKEN_CRED_TYPE)) == 0) {
    credentials.client_id = "";
    credentials.username = data[CREDENTIALS_VALUE].as<std::string>();
    credentials.password = "";
  } else if (strncmp(data[CREDENTIALS_TYPE], MQTT_BASIC_CRED_TYPE, strlen(MQTT_BASIC_CRED_TYPE)) == 0) {
    auto credentials_value = data[CREDENTIALS_VALUE].as<JsonObjectConst>();
    credentials.client_id = credentials_value[CLIENT_ID].as<std::string>();
    credentials.username = credentials_value[CLIENT_USERNAME].as<std::string>();
    credentials.password = credentials_value[CLIENT_PASSWORD].as<std::string>();
  } else {
#ifdef SERIALFEEDBACK
    Serial.printf("Unexpected provision credentialsType: (%s)\n", data[CREDENTIALS_TYPE].as<const char *>());
#endif
    return;
  }

  // Disconnect from the cloud client connected to the provision account, because it is no longer needed the device has been provisioned
  // and we can reconnect to the cloud with the newly generated credentials.
  if (tb.connected()) {
    tb.disconnect();
  }
  provisionResponseProcessed = true;
  for (int i = MEMO; i < credentials.username.length(); i++) {
    EEPROM.write(i, credentials.username[i]);
  }
  EEPROM.write(credentials.username.length(), '\0');
  EEPROM.commit();
}

void loop() {
  if (!reconnect()) {
    return;
  }

  loopTB();
}
