#define FVERSION "2.21"  //version firmware
//A credential file for TB and Wifi must be provided.. otherwise.. the following:
//#define STASSID "SSID"
//#define STAPSK  "password"
//#define UIDdefault "BOXX"
//#define APPSK "passwordAP"
//#define PROVISION_DEVICE_KEY "device_key"
//#define PROVISION_DEVICE_SECRET "device_secret"
//#define THINGSBOARD_SERVER "server URL"

//Tb client can also be activated to send attributes periodically if there is no telemetry (check defintioon ATTRINUTE_ONE)
/*Note the following:
- AP mode is available in http://192.168.4.1/AP 
- Connect first to the AP created with the name of the MAXIBOOSTER (BOXX by default). 
- It is posible to change the WiFi network and the name of the MAXIBOOSTER
- Note that TB will not update the name (entityName), just the attribute (name). 
*/
#include "credentials.h"

#include "webCode.h"
#include "webAP.h"

#include "tb_client.h"
//#define THINGSBOARD_ENABLE_DYNAMIC 1
#include <Arduino_MQTT_Client.h>
#include <Provision.h>
//#define THINGSBOARD_ENABLE_PROGMEM 0  //Needed by Thingboard.h.. otherwise it crashes
#include <ThingsBoard.h>
//arduino json no iba con 6.21.0, sólo con 6.20. Ahora 7.3 OK (warnings)

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>
#include <ESP8266WiFiMulti.h>

#define TIME_TEMPERATURE 3000  //Temperature measure every 3 seconds
#define TIME_WIFI_SERIAL 3000  //Update Wifi Serial every 3 seconds
#define TIME_AP 500            //Time between trying WIFi connection
bool connecting_in_process = false;

//5/1024
//#define CURRENTCONVERSION 0.0048828125
//#define CURRENTCONVERSION 5/1024
#define CURRENTCONVERSION (4.096 / 1024)
//27.73/1024   Mas voltage vit resistor divider 10k 2k2   //22.71 if Vext 4.096
//#define VOLTAGECONVERSION 0.0244140625
//#define VOLTAGECONVERSION 27.73/1024
#define VOLTAGECONVERSION (22.71 / 1024)
//#define VOLTAGECONVERSION 24.71 / 1024

//Note the 27.73 must be included in the cmbus definition...

//Uncoment next define to enable feedback on SERIAL port, otherwise the port will be use to communicate with Wifi module. It can not be used for both
//#define SERIALFEEDBACK
//#define SERIALFEEDBACK2
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

//#include "user_interface.h"

//Option definition fix IP
// the IP address for the shield:
//IPAddress local_IP(192, 168, 1, 222);
// Set your Gateway IP address
//IPAddress gateway(192, 168, 2, 5);
//IPAddress subnet(255, 255, 0, 0);
//IPAddress primaryDNS(8, 8, 8, 8);   //optional
//IPAddress secondaryDNS(8, 8, 4, 4); //optional

String token;
String UIDs, SSIDs, PASSWORDs, TBSERVERs;

struct Credentials {
  std::string client_id;
  //String client_id;
  std::string username;
  //String username;
  std::string password;
  //String password;
};
Credentials credentials;

ESP8266WebServer server(80);

//const int led = 13;
const int led = LED_BUILTIN;
//WEMOS: The built-in led is on pin D4 (arduino pin 2), and it is inverted so if you're pulling D4 LOW anywhere in your sketch the led will be on.
//ESP-01S also built-in led is on pin D4 (arduino pin 2), and it is inverted so if you're pulling D4 LOW anywhere in your sketch the led will be on.

//It seems both modules work with pull-up resistor... so the logic is negative (buttón pushed=0).

const int TemperaturePin = 2;  
const int ButtonPin = 0;       //7Not sure if GPI0 could work. ideally I want GPIO0 for a button
unsigned int temperature;
unsigned char rawCurrent[2];
unsigned char rawVoltage[2];

unsigned long myTimeTemperature, myTimeWifiSerial, myTimeAP;
const int t_wifiSerial = TIME_WIFI_SERIAL;
const int t_temperature = TIME_TEMPERATURE;
const int t_AP = TIME_AP;

const float currentConversion = (float)CURRENTCONVERSION;
const float voltageConversion = (float)VOLTAGECONVERSION;

void handleRoot() {
  String stringOne = pageContent();
  server.send(200, "text/html", stringOne);
#ifdef SERIALFEEDBACK
  Serial.print("temperature: ");
  Serial.println(temperature);
#endif
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

// Initalizes WiFi connection,
// will endlessly delay until a connection has been successfully established

void InitWiFi() {
  // Attempting to establish a connection to the given WiFi network
  if (!connecting_in_process) {
#ifdef SERIALFEEDBACK
    Serial.println("Connecting to AP ...");
#endif
    WiFi.begin(SSIDs.c_str(), PASSWORDs.c_str());
    connecting_in_process = true;
  }

  if (WiFi.status() != WL_CONNECTED) {
    // Delay 500ms until a connection has been succesfully established
    //digitalWrite(led, LOW);
    digitalWrite(led, HIGH);
    if ((millis() - myTimeAP) > t_AP) {
      myTimeAP = millis();
#ifdef SERIALFEEDBACK
      Serial.print(".");
#endif
    };
   }
}
// Reconnects the WiFi uses InitWiFi if the connection has been removed
// Returns true as soon as a connection has been established again
bool reconnect() {
  // Check to ensure we aren't connected yet
  const wl_status_t status = WiFi.status();
  if (status == WL_CONNECTED) {
     //digitalWrite(led, HIGH);
     digitalWrite(led, LOW);
    if (connecting_in_process) {
      connecting_in_process = false;
#ifdef SERIALFEEDBACK
      Serial.print("Connected to AP ");
      Serial.println(SSIDs);
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
#endif
    }
    return true;
  }
  // If we aren't establishing a new connection to the given WiFi network
  InitWiFi();
  return true;
}

void setup(void) {
  Serial.begin(115200);
  delay(2000);
  initMEMO();
  //wdt_disable();
  //ESP.wdtDisable();

  myTimeTemperature = millis();
  myTimeWifiSerial = myTimeTemperature;
  myTimeAP = myTimeTemperature;

  pinMode(ButtonPin, INPUT);
  pinMode(led, OUTPUT);

//  Serial.begin(115200);
  //WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS);

  //setupAP();
  //WiFi.mode(WIFI_STA);
  wifi_set_sleep_type(NONE_SLEEP_T);  //LIGHT_SLEEP_T and MODE_SLEEP_T
  InitWiFi();
  
  if (MDNS.begin(UIDs)){
 #ifdef SERIALFEEDBACK
    Serial.print("MDNS responder started: ");
    Serial.print(UIDs);
    Serial.println(".local");
#endif
  }

  server.on("/", handleRoot);
  server.onNotFound(handleNotFound);

  setupAP();

  server.begin();
  initWebSockets();
#ifdef SERIALFEEDBACK
  Serial.println("HTTP server started");
#endif
  dallas(TemperaturePin, 1);
  temperature = 0;
  rawCurrent[1] = 0;
  rawCurrent[0] = 0;
  rawVoltage[1] = 0;
  rawVoltage[0] = 0;

  setupMBUS();
  initTB();
}

void checkTemperature() {

  if ((millis() - myTimeTemperature) > t_temperature) {
    temperature = dallas(TemperaturePin, 0);  //This takes 5ms... it is too much not to have  small flicker
    myTimeTemperature = millis();
  }
}

void loop(void) {
  //ESP.wdtDisable();

  if (!reconnect()) {
    return;
  }
  MDNS.update();
  server.handleClient();
  processMBUS();
  checkSerialUpdate();
  checkTemperature();
  sendSerialUpdate(false);
  checkButton();
  // if (!connecting_in_process) {loopTB();}
  loopTB();
  //processMBUS();
  webLoop();
}
