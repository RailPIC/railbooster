#include <WiFiUdp.h>
//#include "user_interface.h"
#define BUFFSIZE 20
//#define FVERSION "2.2"  //version firmware
//Hard coded a Gleis Format Prozessor 60213,60214 / Booster 60173, 60174  VERSION 1.0
#define VERSIONM 1
#define VERSIONL 0
#define ESPONTANEUSPING 1

#define BIT_RESPONSE          0x01
#define ID_SYSTEM             0x00
#define CMD_SYSSUB_STOP       0x00
#define CMD_SYSSUB_GO         0x01
#define CMD_SYSSUB_STATUS     0x0B
#define CMD_SYSSUB_OVERLOAD   0x0A
#define CMD_SYSSUB_CLOCK      0x20
#define CAN_ID_PING           0x30
#define ID_SYS_STAT_DATA      0x3A
#define NOHASH     0

#define DELAY_STAT_DATA 400     //It seems if no delay, messages are not arriving.
//Anyway, it seems sometimes messages with meassures.. not STAT_DATAm but SYS... are not arriving to master... problm witu UDP on wifi?
//esp-01 works much worst than wemos for instance (out of maxibboast board too)

bool requestedMBUS[4];  

#define byte_of(your_var,byte_num) (*(((unsigned char*)&your_var)+byte_num))

const unsigned int RXPort = 15735;
const unsigned int TXport = RXPort - 1;
IPAddress multicastRXAddress = IPAddress(224, 0, 0, 250); //This should be also 224.0.0.1. .. but for some reason ESP8266 is not registering well to such multicast address... probably because it is already registered. Registering to another address on the same group is working too.
//IPAddress multicastRXAddress = IPAddress(224, 0, 0, 1); //This should be also 224.0.0.1. .. but for some reason ESP8266 is not registering well to such multicast address... probably because it is already registered. Registering to another address on the same group is working too.
IPAddress multicastTXAddress = IPAddress(224, 0, 0, 1);
WiFiUDP Udp;

unsigned char bufferMSG[BUFFSIZE];
unsigned char zentralUID[4];
unsigned long UID;
unsigned char storedHash[2];
unsigned int minutesActive;
unsigned int minutesLastStatus;
unsigned char state = CMD_SYSSUB_STOP;

/* //Arduino IDE seemns not to need the declarations... actually they cause an error
  void processMBUS();
  bool message4me(unsigned char* msg);
  void parseMgr(unsigned char *msg);
  void sysCMD (unsigned char *msg);
  void sendResponse(unsigned char prio, unsigned char cmd, unsigned char rsp, unsigned char* data, unsigned char length, unsigned int hash);
  void sendStatusConfig(unsigned char index);
  void setOverloadCondition(unsigned char canal = 1);
  void clearOverloadCondition();
*/

void setupMBUS() {

  Udp.beginMulticast(WiFi.localIP(),  multicastRXAddress, RXPort);
  zentralUID[0] = UIDs.charAt(0);  zentralUID[1] = UIDs.charAt(1);  zentralUID[2] = UIDs.charAt(2);  zentralUID[3] = UIDs.charAt(3);

  storedHash[0]  = zentralUID[3] ^ zentralUID[1];
  storedHash[1]  = zentralUID[2] ^ zentralUID[0];
  storedHash[0] |= 0x03; //0bxxxxxx11
  storedHash[1] &= 0x7F; //0b0xxxxxxx

  UID = (zentralUID[0] << 24) + (zentralUID[1] << 16) + (zentralUID[2] << 8) + zentralUID[3];
#ifdef SERIALFEEDBACK
  Serial.print("UID: ");
  Serial.print(UID);
  Serial.print("\t");
  Serial.println(UIDs);
#endif
  minutesActive = 0;
  minutesLastStatus = 0;

for(int i=0;i<4;i++) requestedMBUS[i]=false;

}

void processMBUS() {

  int noBytes = Udp.parsePacket();
  if ( noBytes ) {
#ifdef SERIALFEEDBACK2
    Serial.print(millis() / 1000);
    Serial.print(":Packet of ");
    Serial.print(noBytes);
    Serial.print(" received from ");
    Serial.print(Udp.remoteIP());
    Serial.print(":");
    Serial.println(Udp.remotePort());
#endif
    // We've received a packet, read the data from it
    Udp.read(bufferMSG, noBytes); // read the packet into the buffer

#ifdef SERIALFEEDBACK2
    // display the packet contents in HEX
    for (int i = 1; i <= noBytes; i++) {
      Serial.print(bufferMSG[i - 1], HEX);
      if (i % 32 == 0) {
        Serial.println();
      }
      else Serial.print(' ');
    } // end for
    Serial.println();
#endif
    if (message4me(bufferMSG)) parseMgr(bufferMSG);
  }
}

bool message4me(unsigned char* msg) {
  unsigned long destination = (msg[5] << 24) + (msg[6] << 16) + (msg[7] << 8) + msg[8];
  //printf("destination %lu \n",destination);
  if ((destination == UID) || (destination == 0)) return true;
  else return false;
}

void parseMgr(unsigned char *msg) {

  switch ( msg[1] ) {
    case ID_SYSTEM:
      sysCMD(msg);
      break;
    case CAN_ID_PING: {
        unsigned char data[8];
        for (int i = 0; i < 4; i++)
          data[i] = zentralUID[i];
        data[4] = VERSIONM;    data[5] = VERSIONL;    data[6] = 0x00;    data[7] = 0x00; //Hard coded a Gleis Format Prozessor 60213,60214 / Booster 60173, 60174  VERSION 1.0
        sendResponse(0, CAN_ID_PING, BIT_RESPONSE, data, 8, NOHASH);
#ifdef SERIALFEEDBACK
        Serial.println("PING command sent");
#endif
        break;
      }
    case ID_SYS_STAT_DATA: {
        sendStatusConfig(msg[9]);  //index in msg[9]
        //for (int kk=0;kk<10;kk++) sendStatusConfig(msg[9]);  //index in msg[9] //A ver si mandando más veces ganadmos alfo
        //delay(100); //It seems there are too many messages coming... and in 8266 the messages are not sent well
        //delay(150); //It seems there are too many messages coming... and in 8266 the messages are not sent well
        //delay(250); //It seems there are too many messages coming... and in 8266 the messages are not sent well
        delay(DELAY_STAT_DATA); //It seems there are too many messages coming... and in 8266 the messages are not sent well

#ifdef SERIALFEEDBACK
        Serial.println("SYS_STAT_DATA command sent");
#endif
        break;
      }
    default:
#ifdef SERIALFEEDBACK
      Serial.print("COMMAND not considered: ");
      Serial.println(msg[1], HEX);
#endif
      break;
  }
}

void sysCMD (unsigned char *msg) {

  unsigned char resp_length = 0;

  //Prepare response (same message receive with respbit
  unsigned char data[BUFFSIZE];
  for (int i = 0; i < 4; i++)
    data[i] = zentralUID[i];
  data[4] = msg[9];
  data[5] = msg[10]; //Potentially not needed by some commands
  data[6] = msg[11];
  data[7] = msg[12];

  switch ( msg[9] ) {
    case CMD_SYSSUB_GO:
#ifdef SERIALFEEDBACK
      Serial.println("CMD_SYSSUB_GO command received");
#endif
      resp_length = 5;
      state = CMD_SYSSUB_GO;
      sendSerialUpdate(true);
      break;
    case CMD_SYSSUB_STOP:   //Stop power
#ifdef SERIALFEEDBACK
      Serial.println("CMD_SYSSUB_STOP command received");
#endif
      resp_length = 5;
      state = CMD_SYSSUB_STOP;
      sendSerialUpdate(true);
      break;
    case CMD_SYSSUB_OVERLOAD:  //Message not received.. but to respond
#ifdef SERIALFEEDBACK
      Serial.println("OVERLOAD command sent");
#endif
      resp_length = 6;
      state = CMD_SYSSUB_OVERLOAD;
      break;
    case CMD_SYSSUB_STATUS:
#ifdef SERIALFEEDBACK
      Serial.print("REPORT STATUS CHANNEL: ");
      Serial.println(msg[10]);
#endif
      //if (!overload)   //If overlaod condition.. not sending message
      if (state != CMD_SYSSUB_OVERLOAD) //If overlaod condition.. not sending message
      {
        resp_length = 8;
        switch (msg[10]) {
          case 1: //Current. According to the limits fixed on StatusConfig 5AMP=1024
          case 2: //Amn in ma in PT (for CS2) not clear steps
            data[6] = rawCurrent[1];
            data[7] = rawCurrent[0];
            //data[6] = random(0, 2);
            //data[7] = random(0, 256);

            break;
          case 3: //Volt. //According to the limits fixed on StatusConfig 25V=1024
            data[6] = rawVoltage[1];
            data[7] = rawVoltage[0];
            //data[6] = random(2, 4);
            //data[7] = random(0, 256);
            break;
          case 4: //T in C According to the limits 0-125
            data[6] = 0x00;
            data[7] = (unsigned char)temperature;
            break;
          default:
            data[6] = 0x00;
            data[7] = 0x00;
        }
      }
      minutesLastStatus = minutesActive;
      break;
    case CMD_SYSSUB_CLOCK:
      resp_length = 8;
      minutesActive++;
#ifdef SERIALFEEDBACK
      Serial.print("Minutes active: ");
      Serial.println(minutesActive);
#endif
      //AS far as I can understand: msg[10]=hour msg[11]=minute msg[12]=factor... Not sure for what purpose
      //Assuming the message is received once per minute
      if (minutesActive - minutesLastStatus > ESPONTANEUSPING) {
        //This is a workaound in case there is no master sending pings and RPIC has not being registered by any system capable of launching and treating status data
        //Sending an espontanosu ping will triger the registration process
        //Currently the difference is in 2 monutes (ESPONTANEUSPING). If two minutes without Status messages, this will be triggered
        //The problem is if there is ping but sendStatusConfig was not well received by the sender.. The booster is registered, but without all channels
        //In addition to the spontaneous ping... in case of not receving a CMD_SYSSUB_STATUS for all channels... a sendStatusConfig for the channels not registered
        //should be triggered... I do not know if this will trigger any action in the server...
        //Probably I need seconds since last CMD_SYSSUB_STATUS per channel
        unsigned char ping [BUFFSIZE];
        ping[1] = CAN_ID_PING;
        parseMgr(ping);
      }
      break;
    default:
#ifdef SERIALFEEDBACK
      Serial.print("SYS COMMAND not treated: ");
      Serial.println(msg[9]);
#endif
      break;
  }

  if (resp_length > 0)
    sendResponse(0, ID_SYSTEM, BIT_RESPONSE, data, resp_length, NOHASH);
}

void sendResponse(unsigned char prio, unsigned char cmd, unsigned char rsp, unsigned char* data, unsigned char length, unsigned int hash) {
  //Response is sent through the TX socket.

  int i = 0;
  unsigned char msg[BUFFSIZE];

  //Current Format of the header is working.
  msg[0]  = (prio << 1);
  msg[1]  = cmd;
  msg[1] |= rsp;
  //Hash calculated according to CS2CAN protocol manual
  if (hash == 0) { //
    //      msg[2]  = zentralUID[3]^zentralUID[1];
    //      msg[3]  = zentralUID[2]^zentralUID[0];
    //      msg[2] |= 0x03; //0bxxxxxx11
    //      msg[3] &= 0x7F; //0b0xxxxxxx
    msg[2] = storedHash[0];
    msg[3] = storedHash[1];
  } else {
    msg[2] = byte_of(hash, 1);
    msg[3] = byte_of(hash, 0);
  }
  msg[4]  = length;
  for (i = 0; i < length; i++ )
    msg[5 + i]  = data[i];


  Udp.beginPacketMulticast(multicastTXAddress, TXport, WiFi.localIP());
  Udp.write(msg, length + 5);
  Udp.endPacket();
#ifdef SERIALFEEDBACK2
  Serial.print("Data OUT: ");
  for (i = 0; i < length + 5; i++) {
    Serial.print(msg[i], HEX);
    Serial.print(" ");
  }
  Serial.println("");
#endif
}

void  sendStatusConfig(unsigned char index) {
  unsigned char data[8];

  for (int i = 0; i < 4; i++)
    data[i] = zentralUID[i];
  data[4] = index;
#ifdef SERIALFEEDBACK
      Serial.print("MCS2PARSER: Status Config, index: ");
      Serial.println(index);
#endif
  switch (index) {
    case 0:       //AUTOMATIC REGISTER IS INTIATED WITH CASE 0.. WITHOUT THIS ONE... NO FURTHER MESSAGES TO REGISTER STATUS CAPABILITIES ARE RECEIEECE
      data[5] = 3; //3 packets
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH);
      //3 medidas, número de serie basado en uix BO01, BO02...
      //These seems to be a bug or a misundesrtanding on the protocol. It clearly states the first byte number of meassures and the seond number of canals
      //On v1.0 of the protocol byte 1=0. In any case, any value on byte 0=to a number of canals hereafter makes the canal to be reconfigures. For instance with byt11=1 power=1.
      //This was causing chanel 3 (the original normal configuration) to stop working
      //data[0]=3; data[1]=3; data[2]=0; data[3]=0; data[4]=0xAA;data[5]=0xAA;data[6]=0xAA;data[7]=0xAA;
      //data[0] = 0; data[1] = 1; data[2] = 0; data[3] = 0; data[4] = UID3; data[5] = UID2; data[6] = UID1; data[7] = UID0;
      data[0] = 0; data[1] = 1; data[2] = 0; data[3] = 0; data[4] = zentralUID[0]; data[5] = zentralUID[1]; data[6] =zentralUID[2]; data[7] = zentralUID[3];
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1);
      //Artickle number string 8 bytes,
      //data[0]='6'; data[1]='0'; data[2]='1'; data[3]='1'; data[4]='2';data[5]=0;data[6]=0;data[7]=0;
      //data[0]='6'; data[1]='0'; data[2]='1'; data[3]='7'; data[4]='3';data[5]=0;data[6]=0;data[7]=0;
      //data[0] = 'R'; data[1] = 'A'; data[2] = 'I'; data[3] = 'L'; data[4] = UID3; data[5] = UID2; data[6] = 0; data[7] = 0;
      data[0] = 'R'; data[1] = 'A'; data[2] = 'I'; data[3] = 'L'; data[4] = zentralUID[0]; data[5] = zentralUID[1]; data[6] = 0; data[7] = 0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2);
      //ADevice name (string terminate \0
      //data[0]='T'; data[1]='R'; data[2]='A'; data[3]='C'; data[4]='K';data[5]='0';data[6]='1';data[7]=0;
      //data[0] = 'B'; data[1] = 'O'; data[2] = 'O'; data[3] = 'S'; data[4] = 'T'; data[5] = UID1; data[6] = UID0; data[7] = 0;
      data[0] = 'B'; data[1] = 'O'; data[2] = 'O'; data[3] = 'S'; data[4] = 'T'; data[5] = zentralUID[2]; data[6] = zentralUID[3]; data[7] = 0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3);
      break;
    case 1:
      //case 2:  CURRENT
      data[5] = 5; //5 packets
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH);
      //01 FD 30 F0 E0 C0 00 0F canal 1, potencia -3=0xFD; 4 colors,Nullpunkt (0 2 bytes, not 00 0f)
      data[0] = 1; data[1] = 0xFD; data[2] = 0x30; data[3] = 0xF0; data[4] = 0xE0; data[5] = 0xC0; data[6] = 0; data[7] = 0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1);
      //06 70 06 C2 07 67 08 0C Limits for colours
      //data[0]=0x06; data[1]=0x70; data[2]=0x06; data[3]=0xC2; data[4]=0x07;data[5]=0x67;data[6]=0x08;data[7]=0x0c;
      data[0] = 0x04; data[1] = 0x00; data[2] = 0x06; data[3] = 0x00; data[4] = 0x07; data[5] = 0x68; data[6] = 0x03; data[7] = 0xFF;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2);
      //A54 52 41 43 4B 00 30 2E  SString Messwertbezeichnung \0 Init range (0.
      data[0] = 'T'; data[1] = 'R'; data[2] = 'A'; data[3] = 'C'; data[4] = 'K'; data[5] = 0; data[6] = '0'; data[7] = '.';
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3);
      //30 30 00 32 2E 35 30 00 Init range\0+End range. To my knowledge it isnot fully correct. As Protocol says number of decimals and potencia must be the same
      //data[0]='0'; data[1]='0'; data[2]=0; data[3]='3'; data[4]='.';data[5]='5';data[6]='0';data[7]=0;
      data[0] = '0'; data[1] = '0'; data[2] = '0'; data[3] = 0; data[4] = '4'; data[5] = '.'; data[6] = '0'; data[7] = '9';
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 4);
      //A54 52 41 43 4B 00 30 2E  SString Unit\0
      data[0] = '6'; data[1] = 0; data[2] = 'A'; data[3] = 0; data[4] = 0; data[5] = 0; data[6] = 0; data[7] = 0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 5);
      break;
    case 2:   //VOLTAGE
      //case 1:
      data[5] = 5; //5 packets
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH);
      //03 FD C0 0C 30 C0 00 00 canal 3, potencia -3=0xFD; 4 colors,Nullpunkt
      data[0] = 3; data[1] = 0xFD; data[2] = 0xC0; data[3] = 0x0C; data[4] = 0x30; data[5] = 0xC0; data[6] = 0; data[7] = 0;
      //data[0]=3; data[1]=0x0; data[2]=0xC0; data[3]=0x0C; data[4]=0x30;data[5]=0xC0;data[6]=0;data[7]=0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1);
      //03 9D 04 B2 0A 1E 0C 49 Limits for colours
      //data[0]=0x03; data[1]=0x9D; data[2]=0x04; data[3]=0xB2; data[4]=0x0A;data[5]=0x1E;data[6]=0x0C;data[7]=0x49;
      data[0] = 0x02; data[1] = 0x66; data[2] = 0x03; data[3] = 0x0A; data[4] = 0x03; data[5] = 0x85; data[6] = 0x03; data[7] = 0xFF;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2);
      //56 4F 4C 54 00 31 30 2E  SString Messwertbezeichnung \0 Init range (0.
      //data[0]='V'; data[1]='O'; data[2]='L'; data[3]='T'; data[4]=0;data[5]='1';data[6]='0';data[7]='.';
      data[0] = 'V'; data[1] = 'O'; data[2] = 'L'; data[3] = 'T'; data[4] = 0; data[5] = '0'; data[6] = '.'; data[7] = '0';
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3);
      //30 30 00 32 37 2E 30 30 Init range\0+End range. To my knowledge it isnot fully correct. As Protocol says number of decimals and potencia must be the same
      //data[0]='0'; data[1]='0'; data[2]=0; data[3]='2'; data[4]='5';data[5]='.';data[6]='0';data[7]='0';
//      data[0] = '0'; data[1] = 0; data[2] = '2'; data[3] = '4'; data[4] = '.'; data[5] = '7'; data[6] = '1'; data[7] = 0;
      data[0] = '0'; data[1] = 0; data[2] = '2'; data[3] = '2'; data[4] = '.'; data[5] = '7'; data[6] = '1'; data[7] = 0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 4);
      //00 56 6F 6C 74 00 00 00  END REANGING RATE\0 SString Unit\0
      //data[0]=0; data[1]='V'; data[2]='o'; data[3]='l'; data[4]='t';data[5]=0;data[6]=0;data[7]=0;
      data[0] = 'V'; data[1] = 'o'; data[2] = 'l'; data[3] = 't'; data[4] = 0; data[5] = 0; data[6] = 0; data[7] = 0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 5);
      break;
    case 3:  //TEMPERATURE
      //data[5]=4; //4 packets
      data[5] = 5; //4 packets
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH);
      //04 00 0C 08 F0 C0 00 00 canal 4, potencia 4; 4 colors,Nullpunkt
      data[0] = 4; data[1] = 0; data[2] = 0x0C; data[3] = 0x08; data[4] = 0xF0; data[5] = 0xC0; data[6] = 0; data[7] = 0;
      //data[0]=4; data[1]=0xFE; data[2]=0x0C; data[3]=0x08; data[4]=0xF0;data[5]=0xC0;data[6]=0;data[7]=0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1);
      //00 6B 00 A4 00 CD 00 DB Limits for colours
      //data[0]=0x00; data[1]=0x6B; data[2]=0x00; data[3]=0xA4; data[4]=0x00;data[5]=0xCD;data[6]=0x00;data[7]=0xDB;
      data[0] = 0x00; data[1] = 0x1e; data[2] = 0x00; data[3] = 0x32; data[4] = 0x00; data[5] = 0x64; data[6] = 0x00; data[7] = 0x7D; //7D=125
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2);
      //54 45 4D 50 00 30 2E 30  SString Messwertbezeichnung \0 Init range \0.
      data[0] = 'T'; data[1] = 'E'; data[2] = 'M'; data[3] = 'P'; data[4] = 0; data[5] = '0'; data[6] = '.'; data[7] = '0';
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3);
      //00 38 30 2E 30 00 43 00 Init range\0+End range +uNIT. To my knowledge it isnot fully correct. As Protocol says number of decimals and potencia must be the same
      //data[0]=0; data[1]='8'; data[2]='0'; data[3]='.'; data[4]='0';data[5]=0;data[6]='C';data[7]=0;
      data[0] = 0; data[1] = '1'; data[2] = '2'; data[3] = '5'; data[4] = '.'; data[5] = '0'; data[6] = 0; data[7] = 'C';
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 4);
      data[0] = 0; data[1] = 0; data[2] = 0; data[3] = 0; data[4] = 0; data[5] = 0; data[6] = 0; data[7] = 0;
      sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 5);
      break;
    default:
#ifdef SERIALFEEDBACK
      Serial.println("MCS2PARSER: Status Config, index not defined");
#endif
    break;
  }
requestedMBUS[index]=true;    //Para saber si se ha pedido cada canal
}
/*
  void setOverloadCondition(unsigned char canal){
  unsigned char msg [BUFFSIZE];

  //Any channel is flag as overload.... but is is actually possible to share with channel is overlaoding.
  //Overload boolean allows to stop responding to SYTSEM_DATA.. which is the signal for clearing the overlaod condition
  overload=true;
  //msg[9]=CMD_SYSSUB_STOP;
  //  sysCMD(msg);
  msg[9]=CMD_SYSSUB_OVERLOAD;
  msg[10]=canal;
  sysCMD(msg);
  }

  void clearOverloadCondition(){
  unsigned char msg [BUFFSIZE];

  overload=false;
  //My boosters actually repower after overload (5s).. and overlaod is not possible with no power
  //Some centrals could also sent a power on once they detect again normal operation (rocrail is not working, even if th eoption is flaged)
  msg[9]=CMD_SYSSUB_GO;
  sysCMD(msg);

  }

*/
