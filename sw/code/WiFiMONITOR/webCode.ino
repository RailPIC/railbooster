WebSocketsServer webSocket = WebSocketsServer(81);

void webLoop() {
  webSocket.loop();
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t* payload, size_t length) {

  switch (type) {
    case WStype_DISCONNECTED:
#ifdef SERIALFEEDBACK
        Serial.printf("[%u] Disconnected!\n", num);
#endif
      break;
    case WStype_CONNECTED:
      {
#ifdef SERIALFEEDBACK
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
#endif
      }
      break;
    case WStype_TEXT:    //No compruebo si el texto recibido es "UPDATE"... doy por hecho qe es OK
      {
#ifdef SERIALFEEDBACK
      Serial.printf("[%u] get Text: %s\n", num, payload);
#endif
      String dataJSONweb;
      float current,voltage;
      current=(rawCurrent[1] * 256 + rawCurrent[0])*currentConversion;  
      voltage=(rawVoltage[1] * 256 + rawVoltage[0])*voltageConversion;        
      dataJSONweb = "{\"temperature\": " + String(temperature) + ",\"voltage\": " + String(voltage,3) + ",\"current\": " + String(current,3) + "}";      
      webSocket.sendTXT(num, dataJSONweb);     //Normalmente solo habñra un cliente conectado.. y ademas queremo sque cada cliente gestione su flujo de datos (5 ms o mas)
                                           //((Si no lo suyosería un broadcast a todos los clientes conectados))
      // send data to all connected clients
       //webSocket.broadcastTXT("message here");
      }       
      break;
    case WStype_BIN:
#ifdef SERIALFEEDBACK
      Serial.printf("[%u] get binary length: %u\n", num, length);
      hexdump(payload, length);
#endif
      break;
  }
}

void initWebSockets() {
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}


String pageContent() {
  String title, stringOne;
  float current, voltage;
  title=UIDs+" (v"+FVERSION+")";
  stringOne = "<!DOCTYPE html><html><head><title>" + title + "</title>";
  stringOne += R"(
     <script src="https://cdn.jsdelivr.net/gh/toorshia/justgage/raphael-2.1.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/toorshia/justgage/justgage.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
  </head>
    <body style="text-align: center">
    <div style="display: inline-block" id="temperatureG"></div>
    <div style="display: inline-block" id="currentG"></div>
    <div style="display: inline-block" id="voltageG"></div>
    <canvas id="myChart"></canvas>

    <script>
      // Define los colores que se utilizarán en ambos componentes
      const temperaturaColor = "#FF5733";
      const voltageColor = "#2475B0";
      const corrienteColor = "#EBD329";

      var temperatureG = new JustGage({
        id: "temperatureG",
        value: 0,
        min: 0,
        max: 50,
        title: "Temperatura",
        gaugeColor: "#e6e6e6", 
        levelColors: [temperaturaColor],
        customSectors: [{
          color: temperaturaColor,
          lo: -100,
          hi: 1000
        }]
      });
      
      var voltageG = new JustGage({
        id: "voltageG",
        value: 0,
        min: 0,
        max: 24,
        title: "Voltaje",
        //formatNumber: true,
        decimals: 2,
        gaugeColor: "#e6e6e6", 
        levelColors: [voltageColor],
        customSectors: [{
          color: voltageColor,
          lo: -100,
          hi: 1000
        }]
      });

      var currentG = new JustGage({
        id: "currentG",
        value: 0,
        min: 0,
        max: 5,
        title: "Corriente",
        decimals: 3,
        formatNumber: true,
        gaugeColor: "#e6e6e6", 
        levelColors: [corrienteColor],  
        customSectors: [{
          color: corrienteColor,
          lo: -100,
          hi: 1000
        }]
      });
      

      var ctx = document.getElementById("myChart").getContext("2d");
      var myChart = new Chart(ctx, {
        type: "line",
        data: {
          labels: [], // Aquí colocarás la lista de tiempo generada por el botón
          datasets: [
            {
              label: "Temperatura",
             // backgroundColor: temperaturaColor,
              fill: false,
              borderColor: temperaturaColor,
              data: [],
              yAxisID: "y-axis-1", // Assign to first axis
            },
            {
              label: "Voltage",
              //backgroundColor: voltageColor,
              fill: false,
              borderColor: voltageColor,
              data: [],
              yAxisID: "y-axis-1", // Assign to first axis
            },
            {
              label: "Corriente",
              //backgroundColor: corrienteColor,
              fill: false,
              borderColor: corrienteColor,
              data: [],
              yAxisID: "y-axis-2", // Assign to second axis
            }
          ]
        },
        options: {
          scales: {
            yAxes: [
              {
                id: "y-axis-1",
                position: "left",
                ticks: {
                  beginAtZero: true
                }
              },
              {
                id: "y-axis-2",
                position: "right",
                ticks: {
                  beginAtZero: true,
                   max: 5
                }
              }
            ]
          }
        }
      });

      function getTime(time) {
        var hours = time.getHours();
        var minutes = time.getMinutes();
        var seconds = time.getSeconds();

        return hours + ":" + minutes + "." + seconds;
      };

      socket = new WebSocket("ws:/" + "/" + location.host + ":81");
      socket.onopen = function(e) {
          console.log("[socket] socket.onopen ");
        };
        socket.onerror = function(e) {
          console.log("[socket] socket.onerror ");
        };
        socket.onmessage = function(e) {
          console.log("[socket] " + e.data);
          //document.getElementById("mrdiy_value").innerHTML = e.data;
          parsedData = JSON.parse(e.data);
          temperatureG.refresh(parsedData.temperature);
          voltageG.refresh(parsedData.voltage);
          currentG.refresh(parsedData.current);

        var tiempoActual = new Date(); // Obtener el tiempo actual
        //chart.data.datasets[0].data = [valor1, valor2, valor3];
        myChart.data.datasets[0].data.push(parsedData.temperature);
        myChart.data.datasets[1].data.push(parsedData.voltage);
        myChart.data.datasets[2].data.push(parsedData.current);

        var tiempo = getTime(tiempoActual); // Convierta el tiempo en una cadena legible

        myChart.data.labels.push(tiempo); // Agregar tiempo a la lista de etiquetas x
        myChart.update();
        };


      function actualizarGauges() {
        socket.send('UPDATE');
      }
      
      setInterval(actualizarGauges, 5000);

    </script>
    <button onclick="actualizarGauges();">Actualizar</button>
  </body>
</html>
    
    )";

  return stringOne;
}
