#include "credentials.h" 

void(* resetFunc) (void) = 0; //declare reset function @ address 0

String presentHTML() {
  String stringTwo;
  String title = "<h1>Booster" + UIDs + "</h1>";
  static const String conf_AP = R"(
    <form action='/submit' method='post'>
      <table border='0'>
        <tr><td>UID/Name  <td><input type='text' name='name' value='%UID'><br>
        <tr><td>SSID<td><select style='width: 170px;' name='ssid'>%SSID</select>
        <tr><td>Pwd <td><input type='password' name='pwd'><br>
        <tr><td>TB Server <td><input type='text' name='tbserver' value='%TBSERVER'><br>
      </table>
      <br><input type='submit' value='Submit'>
    </form>
  )";

  stringTwo = title + conf_AP;
  stringTwo.replace("%UID", UIDs);
  stringTwo.replace("%TBSERVER", TBSERVERs);
  String stringThree = scanNetworks();
  stringTwo.replace("%SSID", stringThree);
  return stringTwo;
}
void handleAP() {
  server.send(200, "text/html", presentHTML());
}

void handleSubmit() {
  bool cambio = false;
  //Valores vacios no se guardan... no puede haber ningún valor vacío
  if ((UIDs!=server.arg("name"))&&(server.arg("name")!="")){
    UIDs = server.arg("name");
    cambio = true;}
  if ((SSIDs!=server.arg("ssid"))&&(server.arg("ssid")!="")){
    SSIDs = server.arg("ssid");
    cambio = true;}
  if ((PASSWORDs!=server.arg("pwd"))&&(server.arg("pwd")!="")){
    PASSWORDs = server.arg("pwd");
    cambio = true;}
  if ((TBSERVERs!=server.arg("tbserver"))&&(server.arg("tbserver")!="")){
    TBSERVERs = server.arg("tbserver");
    cambio = true;}
 if (cambio){
    writeUID(); //Everything will be written again with this command... as only paremeter!="" no problems
 }
 //To see something after reset
  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "El dispositivo se reiniciará");
 delay(1000);

 if (cambio) {resetFunc();}  //only reset if cambio

}

void setupAP() {
  //Lo que haga falta para configurar AP
  WiFi.mode(WIFI_AP_STA);  
  WiFi.softAP(UIDs, APPSK);
  server.on("/AP", handleAP);
  server.on("/submit", handleSubmit);
}

String scanNetworks() {
  int n = WiFi.scanNetworks();
  static const String optionn = R"(<option value='%ssidn'selected >%ssidn</option>)";
  String option, options;
  for (int i = 0; i < n; ++i) {
    option = optionn;
    option.replace("%ssidn", WiFi.SSID(i));
    if (SSIDs != WiFi.SSID(i)) {
      option.replace("selected", "");
    }
    options += option;
  }
  return options;
}
