//Tras leer documentación data[1] los 4 MSB tienen el entero y los 4 últimos los decimals 2^-1 2^-2 2^-3 2^-4.. Es decir 1/2 1/4 1/8 1/16  (página 6 del documento)
//se podría recuperar algo de decimales sumando esos 4 ultimos bits

#include <OneWire.h> 
//Sketch from https://www.scargill.net/reading-dallas-ds18b20-chips-quickly/
//It sais I should not try to read the same sensor more tahn every second... I have tried every 10 ms.. and it still works.

int16_t dallas(int x,byte start){ 
  OneWire ds(x);
  byte i;
  byte data[2];
  int16_t result; 
 
  do
  {
    ds.reset(); 
    ds.write(0xCC);
    ds.write(0xBE); 
    for ( i=0; i <2; i++) data[i]=ds.read();
    result=(data[1] <<8) | data[0] ; 
    result>>=4; if (data[1]&128) result|=61440;   //aquí se podría sumar a result como float la parte de los decimales
    if (data[0]&8) ++result;		          //En su lugar se está mirando el bit correspondiente a 1/2 Bit 3=8 Una modificación sencilla sería en lugar de ++result float result2=result +0.5 con 3 ifs más se tendría toda la precisión.
    ds.reset(); 
    ds.write(0xCC);
    ds.write(0x44,1); 
    if (start) delay(1000);
  }while (start--);
  return result;
} 
