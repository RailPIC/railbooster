#include "credentials.h"  //To include in a file of the details to connect to WiFi and TB

//In case of neednot provided in credentials.h



//For some reasons these lines must be set at the beginnign of the main ino file
//#define THINGSBOARD_ENABLE_PROGMEM 0  //Needed by Thingboard.h.. otherwise it crashes
//#include <ThingsBoard.h>
//arduino json v6.20.0... no va con 6.21.0
//#include <Arduino_MQTT_Client.h>
//#include <Provision.h>
//#include <ThingsBoard.h>

//#include <ArduinoJson.h>
//#include <ESP8266WiFi.h>


#define ATTRINUTE_ONE true  //Only send the attributes on the first connection. if there is telemetry.. telemetry will keep the device active.. otherwise...ATTRINUTE_ONE spould be false to keep sending attibutes and keep the device active
//#define PROVISION_TIME 2000  //Time between processing 2000 ms
#define PROVISION_TIME 10000  //Time between processing 2000 ms. PING and STATUS EVERY 5 seconds... avoid the remote possibility of voerlaping every 2 messages or more
//#define PROVISION_TIME 5100  //Time between processing 2000 ms

String DEVICE_NAME;
bool sendATRIBUTES = true;

//Define KEYS for TELEMETRY
#define TEMPERATURE_KEY "temperature"
#define VOLTAGE_KEY "voltage"
#define CURRENT_KEY "current"

//In case not provided in crdentials.h
//#define THINGSBOARD_SERVER "TB-server"
#define THINGSBOARD_PORT 1883U
//#define MAX_MESSAGE_SIZE 256U

#define CREDENTIALS_TYPE "credentialsType"
#define CREDENTIALS_VALUE "credentialsValue"
#define CLIENT_ID "clientId"
#define CLIENT_PASSWORD "password"
#define CLIENT_USERNAME "userName"

WiFiClient espClient;

// Initialize ThingsBoard instance with the maximum needed buffer size
//ThingsBoard tb(espClient, MAX_MESSAGE_SIZE);

// what works message reseive and send=256

//constexpr uint16_t MAX_MESSAGE_SEND_SIZE = 128U;
constexpr uint16_t MAX_MESSAGE_SEND_SIZE = 256U;
//constexpr uint16_t MAX_MESSAGE_RECEIVE_SIZE = 128U;
constexpr uint16_t MAX_MESSAGE_RECEIVE_SIZE = 256U;
constexpr char ACCESS_TOKEN_CRED_TYPE[] = "ACCESS_TOKEN";
constexpr char MQTT_BASIC_CRED_TYPE[] = "MQTT_BASIC";
constexpr uint64_t REQUEST_TIMEOUT_MICROSECONDS = 5000U * 1000U;

// Initalize the Mqtt client instance
Arduino_MQTT_Client mqttClient(espClient);
// Initialize used apis
//const std::array<IAPI_Implementation*, 0U> apis = {};
Provision<> prov;
const std::array<IAPI_Implementation *, 1U> apis = {
  &prov
};
// Initialize ThingsBoard instance with the maximum needed buffer size
ThingsBoard tb(mqttClient, MAX_MESSAGE_RECEIVE_SIZE, MAX_MESSAGE_SEND_SIZE, Default_Max_Stack_Size, apis);


uint32_t previous_processing_time = 0U;

// Statuses for provisioning
bool provisionRequestSent = false;
bool provisionResponseProcessed = false;

void requestTimedOut() {
  Serial.printf("Provision request timed out did not receive a response in (%llu) microseconds. Ensure client is connected to the MQTT broker\n", REQUEST_TIMEOUT_MICROSECONDS);
}

//void processProvisionResponse(const Provision_Data &data) {
void processProvisionResponse(const JsonDocument &data) {
  //int jsonSize = JSON_STRING_SIZE(measureJson(data));
  //Serial.printf("LA GRAN MIERDA");
  const size_t jsonSize = Helper::Measure_Json(data);
  char buffer[jsonSize];
  serializeJson(data, buffer, jsonSize);
#ifdef SERIALFEEDBACK
  Serial.printf("Received device provision response (%s)\n", buffer);
#endif
  if (strncmp(data["status"], "SUCCESS", strlen("SUCCESS")) != 0) {
#ifdef SERIALFEEDBACK
    Serial.printf("Provision response contains the error: (%s)\n", data["errorMsg"].as<const char *>());
#endif
    return;
  }

  if (strncmp(data[CREDENTIALS_TYPE], ACCESS_TOKEN_CRED_TYPE, strlen(ACCESS_TOKEN_CRED_TYPE)) == 0) {
    credentials.client_id = "";
    credentials.username = data[CREDENTIALS_VALUE].as<std::string>();
    credentials.password = "";
  } else if (strncmp(data[CREDENTIALS_TYPE], MQTT_BASIC_CRED_TYPE, strlen(MQTT_BASIC_CRED_TYPE)) == 0) {
    auto credentials_value = data[CREDENTIALS_VALUE].as<JsonObjectConst>();
    credentials.client_id = credentials_value[CLIENT_ID].as<std::string>();
    credentials.username = credentials_value[CLIENT_USERNAME].as<std::string>();
    credentials.password = credentials_value[CLIENT_PASSWORD].as<std::string>();
  } else {
#ifdef SERIALFEEDBACK
    Serial.printf("Unexpected provision credentialsType: (%s)\n", data[CREDENTIALS_TYPE].as<const char*>());
#endif
    return;
  }

  // Disconnect from the cloud client connected to the provision account, because it is no longer needed the device has been provisioned
  // and we can reconnect to the cloud with the newly generated credentials.
  if (tb.connected()) {
    tb.disconnect();
  }
  provisionResponseProcessed = true;
  token = String(credentials.username.c_str());
  writeTOKEN();
}

void initTB() {
  //To be replaced with whatever is needed to get the name
  //DEVICE_NAME="BO02";
  DEVICE_NAME = UIDs;
  //previous_processing_time = millis() - 2000;  //force first init
  previous_processing_time = millis() - PROVISION_TIME-1;  //force first init
  if (readToken()) {
    provisionRequestSent = true;
    provisionResponseProcessed = true;
  } else {
#ifdef SERIALFEEDBACK
    Serial.println("No se encontró token de acceso, realizando self provisioning");
#endif
    provisionRequestSent = false;
    provisionResponseProcessed = false;
  }
}

void telemetryTB() {
#ifdef SERIALFEEDBACK2
  Serial.println("Sending telemetry...");
#endif
  //To be replaced with whatever is needed
  tb.sendTelemetryData(TEMPERATURE_KEY, temperature);
  tb.sendTelemetryData(CURRENT_KEY, (rawCurrent[1] * 256 + rawCurrent[0]) * currentConversion);
  tb.sendTelemetryData(VOLTAGE_KEY, (rawVoltage[1] * 256 + rawVoltage[0]) * voltageConversion);
  //tb.sendTelemetryData("RequestedMBUS_R", requestedMBUS[0]);
  //tb.sendTelemetryData("RequestedMBUS_C", requestedMBUS[1]);
  //tb.sendTelemetryData("RequestedMBUS_V", requestedMBUS[2]);
  //tb.sendTelemetryData("RequestedMBUS_T", requestedMBUS[3]);

}
void attributesTB() {
#ifdef SERIALFEEDBACK
  Serial.println("Sending attributes...");
#endif
  //To be replaced with whatever is needed
  //const int attribute_items = 4;
  //constexpr size_t ATTRIBUTES_SIZE = 2U;
  constexpr size_t attribute_items = 4U;
  IPAddress IP = WiFi.localIP();
  String myIP = WiFi.localIP().toString();

  String ubicacion = "DESCONOCIDA";
  // Comparar la dirección IP con las subredes especificadas
  if (IP[0] == 192 && IP[1] == 168 && IP[2] == 1) {
    ubicacion = "IAIOS";
  } else if (IP[0] == 192 && IP[1] == 168 && IP[2] == 2) {
    ubicacion = "CASA";
  } else if (IP[0] == 192 && IP[1] == 168 && IP[2] == 3) {
    ubicacion = "PERELLO";
  }

  Attribute attributes[attribute_items] = {
    //{"ip",  WiFi.localIP().toString().c_str() },
    { "ip", myIP.c_str() },  //for some reason... is the only thing that works well.. other combination does not send the IP well
    //{ "ip", IP.toString().c_str() },
    //  { "active",       true     },
    { "name", DEVICE_NAME.c_str() },
    { "ubicacion", ubicacion.c_str() },
    { "version", FVERSION }
  };

  Telemetry *begin = attributes;
  Telemetry *end = attributes + attribute_items;
  //tb.sendAttributes(attributes, attribute_items);
  //tb.sendAttributes(begin, end);
  tb.sendAttributes<attribute_items>(begin, end);

#ifdef SERIALFEEDBACK
  Serial.print("IP: ");
  //Serial.println(WiFi.localIP().toString().c_str());
  Serial.println(IP.toString().c_str());
#endif
}

void loopTB() {
  if (((millis() - previous_processing_time) < PROVISION_TIME) || (WiFi.status() != WL_CONNECTED)) {
   // if ((millis() - previous_processing_time < 1000) || (WiFi.status() != WL_CONNECTED)) {
    tb.loop();
    return;
  }
  previous_processing_time = millis();

  
  if (!provisionRequestSent) {
  //Serial.println("MIERDA 01");
  

    if (!tb.connected()) {
      // Connect to the ThingsBoard server as a client wanting to provision a new device
#ifdef SERIALFEEDBACK
      Serial.printf("Provisioning. Connecting to: (%s)\n", TBSERVERs.c_str());
#endif
      //yield();
      if (!tb.connect(TBSERVERs.c_str(), "provision", THINGSBOARD_PORT)) {
#ifdef SERIALFEEDBACK
        Serial.println("Failed to connect");
#endif
        return;
      }
    }
// #if THINGSBOARD_ENABLE_STL
//       std::string device_name = DEVICE_NAME.c_str();
//       //Serial.println("MIERDA");
// #else
//       String device_name = DEVICE_NAME;
// #endif

#ifdef SERIALFEEDBACK
      Serial.println("Sending provisioning request");
      //Serial.println(device_name.c_str());
      Serial.println(DEVICE_NAME.c_str());
#endif
      //std::string device_name = DEVICE_NAME;
      if ((DEVICE_NAME == nullptr) || (DEVICE_NAME[0] == '\0')) {
        DEVICE_NAME = WiFi.macAddress().c_str();
        //device_name = WiFi.macAddress().c_str();
      }

      // Send a claiming request without any device name (access token will be used as the device name)
      // if the string is empty or null, automatically checked by the sendProvisionRequest method
      //const Provision_Callback provisionCallback(Access_Token(), &processProvisionResponse, PROVISION_DEVICE_KEY, PROVISION_DEVICE_SECRET, DEVICE_NAME.c_str());
      //const Provision_Callback provisionCallback(Access_Token(), &processProvisionResponse, PROVISION_DEVICE_KEY, PROVISION_DEVICE_SECRET);
      const Provision_Callback provisionCallback(Access_Token(), &processProvisionResponse, PROVISION_DEVICE_KEY, PROVISION_DEVICE_SECRET, DEVICE_NAME.c_str(), REQUEST_TIMEOUT_MICROSECONDS, &requestTimedOut);
//      const Provision_Callback provisionCallback(Access_Token(), &processProvisionResponse, PROVISION_DEVICE_KEY, PROVISION_DEVICE_SECRET, device_name.c_str(), REQUEST_TIMEOUT_MICROSECONDS, &requestTimedOut);
      //const Provision_Callback provisionCallback(Access_Token(), &processProvisionResponse, PROVISION_DEVICE_KEY, PROVISION_DEVICE_SECRET, device_name.c_str());
      //provisionRequestSent = tb.Provision_Request(provisionCallback);
       // Serial.println("MIERDA2");
      provisionRequestSent = prov.Provision_Request(provisionCallback);
      //Serial.println("MIERDA3");
      //Serial.println(provisionRequestSent);
    //}
  } else if (provisionResponseProcessed) {
    if (!tb.connected()) {
      // Connect to the ThingsBoard server, as the provisioned client
#ifdef SERIALFEEDBACK
      Serial.printf("Connecting to: (%s)\n", TBSERVERs.c_str());
#endif
      if (!tb.connect(TBSERVERs.c_str(), credentials.username.c_str(), THINGSBOARD_PORT, credentials.client_id.c_str(), credentials.password.c_str())) {
#ifdef SERIALFEEDBACK
        Serial.println("Failed to connect");
        Serial.println(credentials.client_id.c_str());
        Serial.println(credentials.username.c_str());
        Serial.println(credentials.password.c_str());
#endif
        //Try provisioning again
        provisionRequestSent = false;
        provisionResponseProcessed = false;
        return;
      } else {
#ifdef SERIALFEEDBACK
        Serial.println("Connected!");
#endif
      }
    } else {
      telemetryTB();
      //Serial.println("Test Tiempo");
      //delay(500);
      if (sendATRIBUTES) {
        delay(500);
        attributesTB();
        if (ATTRINUTE_ONE) sendATRIBUTES = false;
      }
    }
  }
 
  tb.loop();

}
