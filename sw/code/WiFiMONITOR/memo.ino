#include <EEPROM.h>
#include "credentials.h"

#define MEMO 0  //Wich EEPROM position start writing/reading

//bool resetMEMO=false;
bool resetMEMO=false; //workaround to force a reset always

// Función para leer una cadena de la memoria EEPROM a partir de una dirección dada
String readStringFromEEPROM(int address) {
  String s = "";
  char c = EEPROM.read(address);
  int i = 0;
  while (c != '\0') {
    s += c;
    i++;
    c = EEPROM.read(address + i);
  }
  return s;
}

// Función para escribir una cadena en la memoria EEPROM a partir de una dirección dada
void writeStringToEEPROM(String s, int address) {
  int len = s.length();
  for (int i = 0; i < len; i++) {
    EEPROM.write(address + i, s[i]);
  }
  EEPROM.write(address + len, '\0');
  EEPROM.commit();
}

bool readToken() {
  token = readStringFromEEPROM(MEMO);
#ifdef SERIALFEEDBACK
  Serial.print("Token: ");
  Serial.println(token);
#endif
  credentials.username = token.c_str();

  if (token.length() == 0) {
    return false;
    //return 0;
  } else {
    return true;
    //return endToken;
  }
}

void writeTBSERVER() {
  writeStringToEEPROM(TBSERVERs, MEMO + token.length() + UIDs.length() + SSIDs.length() + PASSWORDs.length() + 4);
  //EEPROM.write('\0', MEMO+token.length()+UIDs.length()+SSIDs.length()+PASSWORDs.length()+TBSERVERs.length()+4);
#ifdef SERIALFEEDBACK
  Serial.print("Write TBserver: ");
  Serial.println(TBSERVERs);
#endif
}

void writePASSWORD() {
  writeStringToEEPROM(PASSWORDs, MEMO + token.length() + UIDs.length() + SSIDs.length() + 3);
  //EEPROM.write('\0', MEMO+token.length()+UIDs.length()+SSIDs.length()+PASSWORDs.length()+3);
  writeTBSERVER();
#ifdef SERIALFEEDBACK
  Serial.print("Write Password for SSID: ");
  Serial.println(PASSWORDs);
#endif
}

void writeSSID() {
  writeStringToEEPROM(SSIDs, MEMO + token.length() + UIDs.length() + 2);
  writePASSWORD();
#ifdef SERIALFEEDBACK
  Serial.print("Write SSID: ");
  Serial.println(SSIDs);
#endif
}

void writeUID() {
  writeStringToEEPROM(UIDs, MEMO + token.length() + 1);
  writeSSID();
#ifdef SERIALFEEDBACK
  Serial.print("Write UID: ");
  Serial.println(UIDs);
#endif
}

void writeTOKEN() {
  writeStringToEEPROM(token, MEMO);
  writeUID();
#ifdef SERIALFEEDBACK
  Serial.print("Write token: ");
  Serial.println(token);
#endif
}

void initMEMO() {
  EEPROM.begin(512);
  readToken();
  UIDs = readStringFromEEPROM(MEMO + token.length() + 1);
  SSIDs = readStringFromEEPROM(MEMO + token.length() + UIDs.length() + 2);
  PASSWORDs = readStringFromEEPROM(MEMO + token.length() + UIDs.length() + SSIDs.length() + 3);
  TBSERVERs = readStringFromEEPROM(MEMO + token.length() + UIDs.length() + SSIDs.length() + PASSWORDs.length() + 4);
  // Si las cadenas no están inicializadas, las seteamos a unos valores por defecto (en cascada.. pq hay que correr todo lo que ya existía escrito, ver wwrite)
  if ((UIDs.length() == 0) || (resetMEMO) ){
    UIDs = UIDdefault;
    writeUID();
#ifdef SERIALFEEDBACK
    Serial.println("Init UID");
#endif
  }
  if ((SSIDs.length() == 0) || (resetMEMO) ) {
    SSIDs = STASSID;
    writeSSID();
    #ifdef SERIALFEEDBACK
    Serial.println("Init SSID");
#endif
  }
  if ((PASSWORDs.length() == 0) || (resetMEMO) ) {
    PASSWORDs = STAPSK;
    writePASSWORD();
    #ifdef SERIALFEEDBACK
    Serial.println("Init PASSWORD");
#endif
  }
  if ((TBSERVERs.length() == 0) || (resetMEMO))  {
    TBSERVERs = THINGSBOARD_SERVER;
    writeTBSERVER();
    #ifdef SERIALFEEDBACK
    Serial.println("Init TBSERVER");
#endif
  }
}