#define byte_of(your_var,byte_num) (*(((unsigned char*)&your_var)+byte_num))

//Not passing paremeters and using global variables to speed up processing (ucontroller)

void sendSerialUpdate(bool force) {
  //Serial.println(force);
  if (((millis() - myTimeWifiSerial) > t_wifiSerial) || (force)) {
    unsigned char msg[3];
    msg[0] = state; msg[1]=temperature; msg[2]=!buttonState;
    Serial.write(msg, 3);
    myTimeWifiSerial = millis();
#ifdef SERIALFEEDBACK
    // display the packet contents in HEX
    for (int i = 0; i < 3; i++) {
      Serial.print(msg[i], HEX);
      Serial.print(' ');
    }
    Serial.println();
#endif
  
  }
}

void checkSerialUpdate() {

  unsigned char nbytes = Serial.available();
  unsigned char msg[5];
  if (nbytes) {
    //if (nbytes == 2) {
    //todo OK
    Serial.readBytes(msg, 5);

    rawCurrent[1]=msg[1]; rawCurrent[0]=msg[2]; rawVoltage[1]=msg[3];  rawVoltage[0]=msg[4];
     
    if (state != msg[0]) {
      // state = msg[0];
/*      switch (msg[0]) {
        case CMD_SYSSUB_STOP:
          state = CMD_SYSSUB_STOP;
          //do something.. Send sys command stop
          break;
        case CMD_SYSSUB_GO:
          bitSet(configura, CONFIG_booster);  //Booster Enabled... if activity it will go ON
          break;
        case CMD_SYSSUB_OVERLOAD:
        default: //No overload or othermessage expected from WIFI... OVERLOAD should come from the other part.... Only to inform of temperature
          break;
      }*/

      unsigned char msgBack [BUFFSIZE];
      msgBack[9]=msg[0];
      if (state==CMD_SYSSUB_OVERLOAD) msgBack[10]=1;  //Canal 1 por defecto... no llegan mensajes de overload de otros canales.
      sysCMD(msgBack);
    }
    // } else {
    //más información de la que se quería... se pierde
    while (Serial.available()) Serial.read(); //Empty serial buffer..it should be serial.flush.. but it seems a different meaning
    //}
#ifdef SERIALFEEDBACK
    // display the packet contents in HEX
    Serial.print("longitud Recibida: ");
    Serial.println(nbytes);
    for (int i = 0; i < 5; i++) {
      Serial.print(msg[i], HEX);
      Serial.print(' ');
    }
    Serial.println();
#endif
  }

}
