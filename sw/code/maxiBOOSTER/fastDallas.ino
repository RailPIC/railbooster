#include <OneWire.h> 
//Sketch from https://www.scargill.net/reading-dallas-ds18b20-chips-quickly/
//It sais I should not try to read the same sensor more tahn every second... I have tried every 10 ms.. and it still works.

int16_t dallas(int x,byte start){ 
  OneWire ds(x);
  byte i;
  byte data[2];
  int16_t result; 
 
  do
  {
    ds.reset(); 
    ds.write(0xCC);
    ds.write(0xBE); 
    for ( i=0; i <2; i++) data[i]=ds.read();
    result=(data[1] <<8) | data[0] ; 
    result>>=4; if (data[1]&128) result|=61440; 
    if (data[0]&8) ++result;
    ds.reset(); 
    ds.write(0xCC);
    ds.write(0x44,1); 
    if (start) delay(1000);
  }while (start--);
  return result;
} 
