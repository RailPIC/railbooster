#include <NmraDcc.h>
#include <EEPROM.h>
//#include <OneWire.h>
//#include <DallasTemperature.h>
#include "SevSeg.h"
#include "maxiBOOSTER.h"

//Even with the fastDallas script, reading the temperature sensor, makes the 7 segments to flick

//Uncoment next define to enable feedback on SERIAL port, otherwise the prot will be use to communicate with Wifi module
//#define SERIALFEEDBACK
//Uncomment to reset to factory. Necessary when programming for first time address
//#define RESETFACTORY

//(****) Note that some times it seems CV29 gets corrupted and the getAdrr() function is not able to calculate well the type of decoder... This leads to wrong self-address calculation.
//It can be identified by enabling the serial FEEDBACK. CV1 and CV9 are OK, but the address is not calculated well.

// There is a patch to force a CV29 for Accedssory decoder. It is launched when setting default

//For the future send the Address to WIFIMONITOR to publish in web

//const int DccAckPin = 3 ;
const int DccInPin = 2 ;
const int DccIntPin = 0 ;   //Pin2=INT0
const int DccRailcutPin = 3 ;
//To be review the numbering of the segments
const int SegmentAPin = 4 ;
const int SegmentBPin = 5 ;
const int SegmentCPin = 6 ;
//const int SegmentDPin = 7 ;   //10
const int SegmentDPin = 10 ;   
//const int SegmentEPin = 8 ;  //11
const int SegmentEPin = 11;  
const int SegmentFPin = 9 ;
//const int SegmentGPin = 10 ;  //7
const int SegmentGPin = 7 ;  
//const int SegmentPPin = 11 ;  //8
const int SegmentPPin = 8;  
const int SegmentD1Pin = 12 ;
const int SegmentD2Pin = 14 ;
const int SegmentD3Pin = 15 ;
const int SegmentD4Pin = 16 ;

const int DccEnablePin = 13 ;
/*
const int TemperaturePin = 17 ;
*/
const int SDAPin = 18 ;
const int SCLPin = 19 ;

//***
#define maxCurrentPin A6
#define internalCurrentPin A7
#define internalVoltagePin A3

NmraDcc  Dcc ;
DCC_MSG  Packet ;
byte configura = DEFAULT_CONFIG;
byte configura2 = DEFAULT_CONFIG2;

unsigned long myTime;
unsigned long myTimeShortProtection;
byte nShort = 0;
byte nShortTimeProtection = DEFAULT_T_SHORT_PROTECTION;
byte infoSHOW = 0;
int16_t temperature = 0;
unsigned int   rawMaxCurrent = 0;
unsigned int   rawCurrent = 0;
unsigned int   rawVoltage = 0;
unsigned char state = CMD_SYSSUB_STOP;

unsigned int t_temperature = DEFAULT_T_TEMPERATURE * 100;
unsigned long myTimeTemperature;
unsigned int t_maxCurrent = DEFAULT_T_MAXCURRENT * 100;
unsigned long myTimeMaxCurrent;
unsigned int t_wifiSerial = DEFAULT_T_WIFI_SERIAL * 100;
unsigned long myTimeWifiSerial;

SevSeg sevseg;

const unsigned int myAdrress=myADDRESS;
const float currentConversion=(float)CURRENTCONVERSION;
const float voltageConversion=(float)VOLTAGECONVERSION;
const float powerConversion=(float)POWERCONVERSION;
float voltageCalibration=0;
float calibre1=0;
float calibre2=0;

struct CVPair
{
  uint16_t  CV;
  uint8_t   Value;
};

//Assuming Board Address mode
//address is AAA AAAAAA
//CV_ACCESSORY_DECODER_ADDRESS_LSB is CV1  513 in other NMRA documents
//CV_ACCESSORY_DECODER_ADDRESS_MSB is CV9  521 in other NMRA documents (as CV29 is CV541

CVPair FactoryDefaultCVs [] =
{
  {CV_ACCESSORY_DECODER_ADDRESS_LSB, myADDRESS & 0b00111111},
  {CV_ACCESSORY_DECODER_ADDRESS_MSB, (myADDRESS >> 6 ) & 0b00000111},
  //{CV_ACCESSORY_DECODER_ADDRESS_LSB, myAdrress},
  //{CV_ACCESSORY_DECODER_ADDRESS_MSB, (myAdrress >> 8 ) & 0b00000111},

  {CV_CONFIG, DEFAULT_CONFIG},
  {CV_CONFIG2, DEFAULT_CONFIG2},
  {CV_T_TEMPERATURE, DEFAULT_T_TEMPERATURE},
  {CV_T_MAXCURRENT, DEFAULT_T_MAXCURRENT},
  {CV_T_SHORT_PROTECTION, DEFAULT_T_SHORT_PROTECTION},
  {CV_T_WIFI_SERIAL, DEFAULT_T_WIFI_SERIAL},
  {CV_V_CALIBRE_1, DEFAULT_V_CALIBRE_1},
  {CV_V_CALIBRE_2, DEFAULT_V_CALIBRE_2},
};

//******************************************************************************************************************Supporting Functions************************************************************

void railCUT() {
  digitalWrite( DccRailcutPin, LOW );
  //This delay could be replace by something enabling an action in the middle (not related to measuring consumtion as at this point there should not be consumtion... but maybe temperatur)
  delayMicroseconds( RAILCUTTIME );
  digitalWrite( DccRailcutPin, HIGH );
}

void changePanel(void) {
  float maxCurrent, current, voltage, power;
  switch (infoSHOW) {
    //case CONFIG_Voltage: voltage = rawVoltage * voltageConversion+voltageCalibration; if (voltage<0) voltage=0; sevseg.setNumber(voltage, 2); break;
    case CONFIG_Voltage: voltage = rawVoltage * voltageConversion+voltageCalibration; if (voltage<0) voltage=0; sevseg.setNumberF(voltage, 2); break;
    //case CONFIG_Current: current = rawCurrent * CURRENTCONVERSION; sevseg.setNumber(current, 3); break;
    //case CONFIG_Current: current = rawCurrent * currentConversion; sevseg.setNumber(current, 3); break;
    case CONFIG_Current: current = rawCurrent * currentConversion; sevseg.setNumberF(current, 3); break;
    //case CONFIG_Power: power=rawCurrent * rawVoltage * powerConversion; sevseg.setNumber(power, 2); break;
    case CONFIG_Power: power=rawCurrent * rawVoltage * powerConversion; sevseg.setNumberF(power, 2); break;
    //case CONFIG_MaxCurrent: maxCurrent = rawMaxCurrent * CURRENTCONVERSION; sevseg.setNumber(maxCurrent, 3); break;
    //case CONFIG_MaxCurrent: maxCurrent = rawMaxCurrent * currentConversion; sevseg.setNumber(maxCurrent, 3); break;
    case CONFIG_MaxCurrent: maxCurrent = rawMaxCurrent * currentConversion; sevseg.setNumberF(maxCurrent, 3); break;
    case CONFIG_Temperature: sevseg.setNumber(temperature); break;
    case SHOWCUT: sevseg.setChars("-CUT"); break;
    case SHOWNothing:
    default: sevseg.setChars("----");  
  }
}

void checkActivity(bool activity) {
  if (activity) {
    //myTime=millis();
    if ((nShort == 0) && (bitRead(configura, CONFIG_booster))) {
      myTime = millis();
      digitalWrite( DccEnablePin, HIGH );    //No short condition.. enable=1
      if (state != CMD_SYSSUB_GO) {
        state = CMD_SYSSUB_GO;  //If change in state.. not was already on.. notify inmediatly to WIFI. Previous state maybe short or stop
        sendWIFIupdate(true);
      }
    }
  } else {
    //Serial.println(millis()-myTime, DEC) ;
    if (millis() - myTime > TIMEOUT) {
      digitalWrite( DccEnablePin, LOW ); //Timeout.. enable=0
      if (state == CMD_SYSSUB_GO) {
        state = CMD_SYSSUB_STOP;  //If change in state from go.. not was already stop.. notify inmediatly to WIFI. State==overload should not be removed by a state=STOP
        sendWIFIupdate(true);
      }
    }
  }
}

void checkMaxCurrent(bool force = false); //declaratio to allow defaul paremeter

void checkMaxCurrent(bool force) {
  if (((millis() - myTimeMaxCurrent) > t_maxCurrent) || force) {
    //maxcurrent=analogRead(maxCurrentPin)* MAXCURRENTCONVERSION;
    rawMaxCurrent = analogRead(maxCurrentPin);
    myTimeMaxCurrent = millis();
  }
}

void checkShort() {
  
  //if ((millis() - myTimeMaxCurrent) > t_maxCurrent){  //Test to reduce number of measures... just because the HW is not working properly
  rawCurrent = analogRead(internalCurrentPin);
  rawCurrent=averageCurrent(rawCurrent);
  
  if (rawCurrent > rawMaxCurrent) {
    //short detected
    //0..................MAXnSHORT..................................T_SHORT_PROTECTION
    //Linear measures................measures every 100ms
    //nShort=0 is monitored by checkActivity to restore enable
    //nshort=MAXnSHORT... escalets nSHORT to T_SHORT_PROTECTION
    //nshort>MAXnSHORT is monitored by the loop to only measure every 100ms.

    //    if (nShort<MAXnSHORT) nShort++;
    //    if (nShort==MAXnSHORT) nShort=nShortTimeProtection;
    //    if ((nShort>MAXnSHORT)&&(nshort<nShortTimeProtection)) nShort++;

    if ((nShort < MAXnSHORT) || ((nShort > MAXnSHORT) && (nShort < nShortTimeProtection))) nShort++;
    else {
      nShort = nShortTimeProtection;
      digitalWrite( DccEnablePin, LOW );
      infoSHOW = SHOWCUT;
      if (state!=CMD_SYSSUB_OVERLOAD) {  //only send shortcut first time it is detected inmediatly, otherwise it will be sent in every t_wifi_update as usual (as part of the main loop)
        state = CMD_SYSSUB_OVERLOAD;
      sendWIFIupdate(true);
      }
    }

  } else {
    //no short
    if (nShort > 0) nShort--;
    else { //nShort=0 restore Panel... enable only if there is activity
      if (state==CMD_SYSSUB_OVERLOAD){  //restore panel and send signal no shortcut (default stop). If already not shortcut... then no need to resotre anything
      switch (configura & (~DEFAULT_MASK_CONFIG)) {
        case 2: infoSHOW = CONFIG_Voltage; break;
        case 4: infoSHOW = CONFIG_Current; break;
        case 8: infoSHOW = CONFIG_Power; break;
        case 16: infoSHOW = CONFIG_MaxCurrent; break;
        case 32: infoSHOW = CONFIG_Temperature; break;
        default: infoSHOW = SHOWNothing;
      }
      state = CMD_SYSSUB_STOP; //By default we come back to stop position.. if activity... it will quickly move to GO position
      sendWIFIupdate(true);
    }}
  }
  //}
}

void checkVoltage(){
  //Currently no control over coltage or under voltage
  rawVoltage = analogRead(internalVoltagePin);
  // Serial.print ("VoltageAnalog: ");
  // Serial.println (rawVoltage) ;
  // Serial.print ("VoltageAnalogConversion: ");
  // Serial.println (rawVoltage* voltageConversion) ;
   rawVoltage=averageVoltage(rawVoltage);
  // Serial.print("VoltajeAverageConversion: ") ;
  // Serial.println (rawVoltage * voltageConversion+voltageCalibration) ;
 //  Serial.println (rawVoltage * voltageConversion) ;
  }
/*

void checkTemperature() {
  //temp_sensor.requestTemperatures();
  //temperature=temp_sensor.getTempCByIndex(0);
  //int16_t tempo_temperature;

  if ((millis() - myTimeTemperature) > t_temperature) {
    //sevseg.refreshDisplay();
    temperature = dallas(TemperaturePin, 0);  //This takes 5ms... it is too much not to have  small flicker
    //sevseg.refreshDisplay();
    myTimeTemperature = millis();
  }
}
*/
//******************************************************************************************************************DCC************************************************************

uint8_t FactoryDefaultCVIndex = 0;

//void(* resetFunc) (void) = 0;//declare reset function at address 0
//https://www.instructables.com/two-ways-to-reset-arduino-in-software/

void notifyCVResetFactoryDefault()
{
  // Make FactoryDefaultCVIndex non-zero and equal to num CV's to be reset
  // to flag to the loop() function that a reset to Factory Defaults needs to be done
  FactoryDefaultCVIndex = sizeof(FactoryDefaultCVs) / sizeof(CVPair);
};

// This function is called by the NmraDcc library when a DCC ACK needs to be sent
// Calling this function should cause an increased 60ma current drain on the power supply for 6ms to ACK a CV Read
void notifyCVAck(void)
{
#ifdef SERIALFEEDBACK
  Serial.println("notifyCVAck") ;
#endif

  //In the design no available pins. Replaced for a hihc onsumtion on 7sgements or TFT: 8.8.8.8 Not sure if it will work

  sevseg.setChars("8.8.8.8");
  sevseg.refreshDisplay();

  /*
    digitalWrite( DccAckPin, HIGH );
    delay( 6 );
    digitalWrite( DccAckPin, LOW );
  */
}

void notifyDccMsg( DCC_MSG * Msg)
{
  if (bitRead(configura, CONFIG_cutout)) railCUT();
  checkActivity(true);
  //checkShort();
  //if (bitRead(configura2, CONFIG2_Read_Temperature)) checkTemperature();
  changePanel();
  sendWIFIupdate(false);
  if (bitRead(configura2, CONFIG2_Read_Voltage)) checkVoltage();  //Lasyt to give sometimes after railcurt
  /*
    #ifdef SERIALFEEDBACK
    Serial.print("notifyDccMsg: ") ;
    for(uint8_t i = 0; i < Msg->Size; i++)
    {
      Serial.print(Msg->Data[i], HEX);
      Serial.write(' ');
    }
    Serial.println();
    #endif
  */
}

void notifyDccIdle( DCC_MSG * Msg)
{
  if (bitRead(configura, CONFIG_cutout)) railCUT();
  checkActivity(true);
  //checkShort()checkShort();
  //if (bitRead(configura2, CONFIG2_Read_Temperature)) checkTemperature();
  changePanel();
  sendWIFIupdate(false);
  if (bitRead(configura2, CONFIG2_Read_Voltage)) checkVoltage();
  /*
    #ifdef SERIALFEEDBACK
    Serial.print("notifyDccMsg: ") ;
    for(uint8_t i = 0; i < Msg->Size; i++)
    {
      Serial.print(Msg->Data[i], HEX);
      Serial.write(' ');
    }
    Serial.println();
    #endif
  */
}


// This function is called whenever a normal DCC Turnout Packet is received and we're in Board Addressing Mode
void notifyDccAccTurnoutBoard( uint16_t BoardAddr, uint8_t OutputPair, uint8_t Direction, uint8_t OutputPower )
{

  //Serial.println(Dcc.getAddr(), DEC) ;
#ifdef SERIALFEEDBACK
  Serial.print("notifyDccAccTurnoutBoard: ") ;
  Serial.print(BoardAddr, DEC) ;
  Serial.print(',');
  Serial.print(OutputPair, DEC) ;
  Serial.print(',');
  Serial.print(Direction, DEC) ;
  Serial.print(',');
  Serial.println(OutputPower, HEX) ;
#endif

  //Only my address packets are being proccess because of the flag:
  //configura=configura&
  byte configuraBit = 2 * OutputPair + Direction;

  if ((configuraBit > CONFIG_booster) && (configuraBit < CONFIG_cutout)) {
    //change on visualization, special treatment
    configura = configura & DEFAULT_MASK_CONFIG; //Eveything to zero.. nothing to show
    //The following two lines facilitate the
    if (OutputPower) infoSHOW = configuraBit;
    else infoSHOW = SHOWNothing;

  }
  bitWrite(configura, configuraBit, OutputPower);

  if (EEPROM.read(CV_CONFIG) != configura ) EEPROM.write(CV_CONFIG, configura) ;

#ifdef SERIALFEEDBACK
  Serial.print("configura: ");
  Serial.println(configura, BIN) ;
  Serial.print("infoSHOW: ");
  Serial.println(infoSHOW) ;
#endif


}

//I need to call this to treat the CONFIG_CV_Opened bit.

uint8_t notifyCVWrite( uint16_t CV, uint8_t Value) {

#ifdef SERIALFEEDBACK
  Serial.print("notifyCVWrite. CV: ") ;
  Serial.print(CV, DEC) ;
  Serial.print(" , Value: ");
  Serial.println(Value, DEC) ;
#endif
  //Error setCv call notifyCVWrite... infinite loop
  //Dcc.setCV(CV,Value);

  //The DCC library has a WriteCV function to actually set the CV... unfortunatelly, it is not available at the DCC object... It is necessary to reproduce its functionallity hereafter (modify with the special need to disable access to CV of a booster unit.
  //The booster must be able to forward transparently service messagses. This is doneby activating/disactivating the CONFIG_CV_Opened
  //Serial.println(configura,BIN) ;
  if (( EEPROM.read(CV) != Value ) && bitRead(configura, CONFIG_CV_Opened)) //Only write if CV are opened to write
  {
    EEPROM.write(CV, Value) ;
    /*
        if( notifyCVChange )
          notifyCVChange( CV, Value) ;

        if( notifyDccCVChange && !(DccProcState.Flags & FLAGS_SETCV_CALLED) )
          notifyDccCVChange( CV, Value );
    */
    sevseg.setNumber(CV); 
    for (byte h=0;h<200;h++){
      sevseg.refreshDisplay();
      delay(5);
      }
    delay(2000);
    sevseg.setNumber(Value); 
    for (byte h=0;h<200;h++){
      sevseg.refreshDisplay();
      delay(5);
      }
    
    switch (CV) {
      case CV_CONFIG2: configura2 = Value; break;
      case CV_T_TEMPERATURE: t_temperature = Value * 100; break;
      case CV_T_MAXCURRENT: t_maxCurrent = Value * 100; break;
      case CV_T_SHORT_PROTECTION: nShortTimeProtection = Value;  break; //not the us, but the value... the idea is to read nShortTimeProtection every 100ms
      case CV_T_WIFI_SERIAL: t_wifiSerial = Value * 100;  break;
      case CV_V_CALIBRE_1:
        if (Value>127) calibre1=(Value-128)*(-1); //Negative calibration
        else calibre1=Value;
        voltageCalibration=calibre1*calibre2/100;
        break;
      case CV_V_CALIBRE_2:
        if (Value>127) calibre2=(Value-128)*(-1); //Negative calibration
        else calibre2=Value;
        voltageCalibration=calibre1*calibre2/100;
        break;
    }

    //    if (CV == CV_CONFIG2) configura2 = Value;
    //    if (CV == CV_T_TEMPERATURE) t_temperature = Value * 100;
    //    if (CV == CV_T_MAXCURRENT) t_maxCurrent = Value * 100;
    //    if (CV == CV_T_SHORT_PROTECTIONT) nShortTimeProtection=Value;  //not the us, but the value... the idea is to read nShortTimeProtection every 100ms
  }

  return EEPROM.read(CV) ;
}

void    notifyServiceMode(bool estado) {

#ifdef SERIALFEEDBACK
  Serial.print("ServiceMode: ") ;
  Serial.println(estado, DEC);
#endif
}

//************************************************************************SETUP**************************************

void setup()
{
#ifdef RESETFACTORY
//patch  for CV29 corruption and therefore bad getAdrr(). CV29 must be set before init the DCC
    for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0);
  }
   EEPROM.write(29, 128);  
#endif

  Serial.begin(115200);
    // Configure the DCC CV Programing ACK pin for an output
  //pinMode( DccAckPin, OUTPUT );

#ifdef SERIALFEEDBACK
  Serial.println("NMRA DCC Booster");
  Serial.print("CV 1: ");
  Serial.println(Dcc.getCV(CV_ACCESSORY_DECODER_ADDRESS_LSB)) ;
  Serial.print("CV 9: ");
  Serial.println(Dcc.getCV(CV_ACCESSORY_DECODER_ADDRESS_MSB)) ;
  Serial.print("CV 29: ");
  Serial.println(Dcc.getCV(29)) ;
  Serial.print("Init Done in address: ");
  Serial.println(Dcc.getAddr(), DEC) ;
#endif
  Dcc.pin(DccIntPin, DccInPin, 1);

 //EEPROM.write(29, 0);
  // Call the main DCC Init function to enable the DCC Receiver
  //Dcc.init( MAN_ID_DIY, 10, CV29_ACCESSORY_DECODER | CV29_OUTPUT_ADDRESS_MODE, 0 );
 
  //Dcc.initAccessoryDecoder( MAN_ID_DIY, 10, FLAGS_MY_ADDRESS_ONLY, 0 );
  //Dcc.init( MAN_ID_DIY, 10, CV29_ACCESSORY_DECODER, 0 );
 
 Dcc.init( MAN_ID_DIY, 10, CV29_ACCESSORY_DECODER | FLAGS_MY_ADDRESS_ONLY, 0 );
 //Dcc.init( MAN_ID_DIY, 10, 0, 0 );

#ifdef SERIALFEEDBACK
  Serial.print("CV 1: ");
  Serial.println(Dcc.getCV(CV_ACCESSORY_DECODER_ADDRESS_LSB)) ;
  Serial.print("CV 9: ");
  Serial.println(Dcc.getCV(CV_ACCESSORY_DECODER_ADDRESS_MSB)) ;
  Serial.print("Init Done in address: ");
  Serial.println(Dcc.getAddr(), DEC) ;
    
#endif
  // Uncomment both lines to force CV Reset to Factory Defaults

#ifdef RESETFACTORY
  notifyCVResetFactoryDefault();
  configura = 0b11111111; //ALL ENABLE
#endif

  //Script to init factory default if rquested out of the loop (firt time factory defaults)
  while  (FactoryDefaultCVIndex) {
    if ( Dcc.isSetCVReady())
    {
      FactoryDefaultCVIndex--;
      Dcc.setCV( FactoryDefaultCVs[FactoryDefaultCVIndex].CV, FactoryDefaultCVs[FactoryDefaultCVIndex].Value);
#ifdef SERIALFEEDBACK
      Serial.print(FactoryDefaultCVs[FactoryDefaultCVIndex].CV);
      Serial.print(" ");
      Serial.print(FactoryDefaultCVs[FactoryDefaultCVIndex].Value) ;
      Serial.print(" ");Serial.println(Dcc.getCV(FactoryDefaultCVs[FactoryDefaultCVIndex].CV)) ;
#endif
    }
  }

#ifdef SERIALFEEDBACK
  Serial.print("Final Init Done in address: ");
  Serial.println(Dcc.getAddr(), DEC) ;
#endif


  if ( Dcc.isSetCVReady()) configura = Dcc.getCV(CV_CONFIG);
  pinMode( DccRailcutPin, OUTPUT );
  digitalWrite( DccRailcutPin, HIGH );
  pinMode( DccEnablePin, OUTPUT );
  digitalWrite( DccEnablePin, LOW );    //At first no signal
  analogReference(EXTERNAL);
  //activePanel=(configura&(~DEFAULT_MASK_CONFIG))>>1;
  switch (configura & (~DEFAULT_MASK_CONFIG)) {
    case 2: infoSHOW = CONFIG_Voltage; break;
    case 4: infoSHOW = CONFIG_Current; break;
    case 8: infoSHOW = CONFIG_Power; break;
    case 16: infoSHOW = CONFIG_MaxCurrent; break;
    case 32: infoSHOW = CONFIG_Temperature; break;
    default: infoSHOW = SHOWNothing;
  }

  if ( Dcc.isSetCVReady()) configura2 = Dcc.getCV(CV_CONFIG2);
  if ( Dcc.isSetCVReady()) t_temperature = Dcc.getCV(CV_T_TEMPERATURE) * 100;
  if ( Dcc.isSetCVReady()) t_maxCurrent = Dcc.getCV(CV_T_MAXCURRENT) * 100;
  if ( Dcc.isSetCVReady()) nShortTimeProtection = Dcc.getCV(CV_T_SHORT_PROTECTION);
  if ( Dcc.isSetCVReady()) t_wifiSerial = Dcc.getCV(CV_T_WIFI_SERIAL) * 100;
  if ( Dcc.isSetCVReady()) {
    calibre1 = Dcc.getCV(CV_V_CALIBRE_1);
    if (calibre1>127) calibre1=(calibre1-128)*(-1); //Negative calibration
    voltageCalibration=calibre1*calibre2/100;
  }
   if ( Dcc.isSetCVReady()) {
    calibre2 = Dcc.getCV(CV_V_CALIBRE_2);
    if (calibre2>127) calibre2=(calibre2-128)*(-1); //Negative calibration
    voltageCalibration=calibre1*calibre2/100;
  }
//}

#ifdef SERIALFEEDBACK
  Serial.print("Configura: ");
  Serial.println(configura, BIN) ;
  Serial.print("Configura2: ");
  Serial.println(configura2, BIN) ;
  Serial.print("Calibra: ");
  Serial.println(voltageCalibration) ;
  Serial.print("Calibre1: ");
  Serial.println(calibre1) ;
  Serial.print("Calibre2: ");
  Serial.println(calibre2) ;
  Serial.print("t_temperature: ");
  Serial.println(t_temperature) ;
  Serial.print("t_maxCurrent: ");
  Serial.println(t_maxCurrent) ;
    Serial.print("nShortTimeProtection: ");
  Serial.println(nShortTimeProtection) ;
    Serial.print("t_wifiSerial: ");
  Serial.println(t_wifiSerial) ;
    Serial.print("currentConversion: ");
  Serial.println(currentConversion,8) ;
    Serial.print("voltageConversion: ");
  Serial.println(voltageConversion,8) ;
    Serial.print("powerConversion: ");
  Serial.println(powerConversion,8) ;

  

#endif

  //temp_sensor.begin();
  //dallas(TemperaturePin, 1);

  byte numDigits = 4;
  byte digitPins[] = {SegmentD1Pin, SegmentD2Pin, SegmentD3Pin, SegmentD4Pin};
  byte segmentPins[] = {SegmentAPin, SegmentBPin, SegmentCPin, SegmentDPin, SegmentEPin, SegmentFPin, SegmentGPin, SegmentPPin};
  bool resistorsOnSegments = false; // 'false' means resistors are on digit pins
  //byte hardwareConfig = COMMON_ANODE; // See README.md for options
  byte hardwareConfig = COMMON_CATHODE; // See README.md for options
  bool updateWithDelays = false; // Default. Recommended
  bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros

  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments, updateWithDelays, leadingZeros);
  sevseg.setBrightness(150);
  sevseg.setChars("----");
  //I will use the default millis option to manage timing.. hope the DCC lñibrary is not interfering with the nomral behaviour
  //According to https://www.arduino.cc/reference/en/language/functions/time/millis/ millis will only overflow after 50 days... so no problem in this use
  //micros would overflow after 70 minutes https://www.arduino.cc/reference/en/language/functions/time/micros/

  checkMaxCurrent(true);

  myTime = millis();
  //myTimeTemperature = myTime;
  myTimeShortProtection = myTime;
  myTimeWifiSerial = myTime;
  nShort = 0;
  initAverages();
}

//unsigned long myTime;
void loop()
{
  //Serial.print("IN: ");
  //Serial.println(millis());
  // You MUST call the NmraDcc.process() method frequently from the Arduino loop() function for correct library operation
  Dcc.process();
  sevseg.refreshDisplay(); // Must run repeatedly

  if ( FactoryDefaultCVIndex && Dcc.isSetCVReady())
  {
    FactoryDefaultCVIndex--; // Decrement first as initially it is the size of the array
    Dcc.setCV( FactoryDefaultCVs[FactoryDefaultCVIndex].CV, FactoryDefaultCVs[FactoryDefaultCVIndex].Value);
  }

  checkActivity(false);
  //changePanel();

  if (nShort < MAXnSHORT) checkShort(); //If no short condition... check short constantly
  else if (millis() - myTimeShortProtection > 100) {
    checkShort();  //If short condition check every 100 ms ... depending on the starting point it force by defaul 5seconds of short status
    myTimeShortProtection = millis();
  }
  
  checkWIFIupdate();
  
  //si no enable checkLimit para configurar... se tiene que mostrar en 7segmentos tb
  if (!(digitalRead(DccEnablePin))) {   //If booster off
    //if (bitRead(configura2, CONFIG2_Read_Temperature)) checkTemperature();
    checkMaxCurrent();
    changePanel();
    if (bitRead(configura2, CONFIG2_Read_Voltage)) checkVoltage();
    sendWIFIupdate(false);
#ifdef SERIALFEEDBACK
    // Serial.print("Temperature: ");
    //Serial.println(temperature) ;
#endif
  //Serial.print("OUT: ");
  //Serial.println(millis());
  }

}
