#define byte_of(your_var,byte_num) (*(((unsigned char*)&your_var)+byte_num))

//Not passing paremeters and using global variables to speed up processing (ucontroller)

void sendWIFIupdate(bool force) {
  //Serial.println(force);
  if (((millis() - myTimeWifiSerial) > t_wifiSerial) || (force)) {
    unsigned char msg[5];
    msg[0] = state;  msg[1] = byte_of(rawCurrent, 1);  msg[2] = byte_of(rawCurrent, 0);  msg[3] = byte_of(rawVoltage, 1);  msg[4] = byte_of(rawVoltage, 0);
    //msg[0] = state;  msg[1] = random(0, 2);  msg[2] = random(0, 256);  msg[3] = random(2, 4);  msg[4] = msg[2];
    //msg[0] = state;  msg[1] = 0;  msg[2] = 1;  msg[3] = 3;  msg[4] = msg[2];
    //msg[0] = "H";  msg[1] = "O";  msg[2] = "L";  msg[3] = "A";  msg[4] = "M";
    Serial.write(msg, 5);
    myTimeWifiSerial = millis();
#ifdef SERIALFEEDBACK
    // display the packet contents in HEX
    for (int i = 0; i < 5; i++) {
      Serial.print(msg[i], HEX);
      Serial.print(' ');
    }
    Serial.println();
#endif
  }
}

void checkWIFIupdate() {

  unsigned char nbytes = Serial.available();
  unsigned char msg[3];
  if (nbytes) {
    //if (nbytes == 2) {
    //todo OK
    Serial.readBytes(msg, 3);   //This already includes a timeout of 1s by default Serial.setTimeout() to read two bytes in case only the first one has arrived (nbytes=1)

    temperature = msg[1];

    //update Infoshow on button selection at WiFI
    if (msg[2]){
      if (infoSHOW<CONFIG_Temperature) infoSHOW++;
      else infoSHOW=0;
    }
   
    if (state != msg[0]) {
      //desactivar booster o activar (config cv).. aunque no se guarde en eprom... en powerogff, desactivar tb enable.. en on. no.. que se active si hay actividad.
      switch (msg[0]) {
        case CMD_SYSSUB_STOP:
          state = CMD_SYSSUB_STOP;
          bitClear(configura, CONFIG_booster);    //Boster disabled  In 100ms wil go OF because it will not detect activity (timeout)
          digitalWrite( DccEnablePin, LOW );      // enable=0 To speed up and not wait 100ms
          break;
        case CMD_SYSSUB_GO:
          bitSet(configura, CONFIG_booster);  //Booster Enabled... if activity it will go ON
          //digitalWrite( DccEnablePin, HIGH );      // enable=0 To speed up and not wait 100ms
          break;
        case CMD_SYSSUB_OVERLOAD:
        default: //No overload or othermessage expected from WIFI... OVERLOAD should come from the other part.... Only to inform of temperature
          break;
      }
    }
    // } else {
    //más información de la que se quería... se pierde
    while (Serial.available()) Serial.read(); //Empty serial buffer..it should be serial.flush.. but it seems a different meaning
    //}
#ifdef SERIALFEEDBACK
    // display the packet contents in HEX
    Serial.print("longitud Recibida: ");
    Serial.println(nbytes);
    for (int i = 0; i < 2; i++) {
      Serial.print(msg[i], HEX);
      Serial.print(' ');
    }
    Serial.println();
#endif
  }

}
