#ifdef SERIALFEEDBACK
#define AVERAGECURRENT  2 
#define AVERAGEVOLTAGE  2
#else
#define AVERAGECURRENT  512 
#define AVERAGEVOLTAGE  128  //Even if there is room for 256... and it compiles.. then it is note working in most of chips... (less memory?)

#endif
//with unsigned int for rawCurrent only 64 max Average (1024^*64=65.536).. max integer... That is the reason of moving to suma long

//Otra forma de reducir el tiempo de bucle de suma... Evitar bucle, restar a suma ya hecha el valor más viejo (en index) y sumar el valor index nuevo, La suma cada ez solo varía en el valor antiguo que desaparece y el nuevo que entra
//Haciendo sumas totals.. parece que llego a un 1.02% de timelimite=1... Con el truco de la resta y la suma.. 0.51.. bajar de esto parece no posible... ni haciendo solo 16... parece ser la desviación estadistica del cambio de mil
//Con bucle de suma sí que puede haber cambios al reducir el número de medias...de 128 a 64 sí que baja... bajar de 64 no sentido. Cambiar a byte los indices no ahorra tampoco mucho... //bajar sima a int en luhgar de long (no valido para avergae>64 sí que arregla un poco.. peor no mucho


unsigned int rawCurrents[AVERAGECURRENT];
//byte indexCurrent=0;
unsigned int indexCurrent = 0;
unsigned long sumaC = 0;

unsigned int rawVoltages[AVERAGEVOLTAGE];
//byte indexVoltage=0;
unsigned int indexVoltage = 0;
unsigned long sumaV = 0;

void initAverages() {
  //  for (byte i=0;i<AVERAGECURRENT;i++) rawCurrents[i]=0;
  //  for (byte i=0;i<AVERAGEVOLTAGE;i++) rawVoltages[i]=0;

  for (unsigned int i = 0; i < AVERAGECURRENT; i++) {
    rawCurrents[i] = 0;
    sumaC += rawCurrents[i];
  }
  for (unsigned int i = 0; i < AVERAGEVOLTAGE; i++) {
    rawVoltages[i] = 0;
    sumaV += rawVoltages[i];
  }

}

unsigned int averageCurrent(unsigned int newValue) {
  //unsigned int suma=0;
  //unsigned long suma=0;

  sumaC = sumaC - rawCurrents[indexCurrent]; //Quitamos el último valor de la última suma
  rawCurrents[indexCurrent] = newValue;
  sumaC = sumaC + newValue;              //añadimos a la suma el valor último  //Evitando largo bucle de sumas

  if (indexCurrent == (AVERAGECURRENT - 1)) indexCurrent = 0;
  else indexCurrent++;

  // for (byte i=0; i<AVERAGECURRENT;i++) suma+=rawCurrents[i];
  //for (unsigned int i=0; i<AVERAGECURRENT;i++) suma+=rawCurrents[i];

  return sumaC / AVERAGECURRENT;

}

unsigned int averageVoltage(unsigned int newValue) {
  //unsigned int suma=0;
  //unsigned long suma=0;

  sumaV = sumaV - rawVoltages[indexVoltage]; //Quitamos el último valor de la última suma
  rawVoltages[indexVoltage] = newValue;
  sumaV = sumaV + newValue;              //añadimos a la suma el valor último  //Evitando largo bucle de sumas
  if (indexVoltage == (AVERAGEVOLTAGE - 1)) indexVoltage = 0;
  else indexVoltage++;

  //for (byte i=0; i<AVERAGEVOLTAGE;i++) suma+=rawVoltages[i];
  //for (unsigned int i=0; i<AVERAGEVOLTAGE;i++) suma+=rawVoltages[i];

  return sumaV / AVERAGEVOLTAGE;

}
