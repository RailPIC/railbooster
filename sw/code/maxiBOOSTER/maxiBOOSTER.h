//It would be still covinient to only measure temperature every 2-5 seconds

//I will use for Maxibooster addresses 500-511  CV1=52,53,54..; CV9=7
#define myADDRESS 500
// CV_ACCESSORY_DECODER_ADDRESS_LSB       1  myADDRESS & 0b00111111
// CV_ACCESSORY_DECODER_ADDRESS_MSB       9  (myADDRESS >> 6 ) & 0b00000111

#define RAILCUTTIME 415 //in us
//#define TIMEOUT           30 //After 30ms Maxibooster should go off
#define TIMEOUT           250 //MMX requires a larger cycle 100 is not working eaither
//#define TIMESHORT         5000  //After 5 seconds cutout can be recovered
#define MAXnSHORT           5    //After 5 short detected, launch short signal
//5/1024
//#define CURRENTCONVERSION 0.0048828125 
//#define CURRENTCONVERSION 5/1024
//#define CURRENTCONVERSION 4.096/1024
#define CURRENTCONVERSION (4.096/1024)
//27.73/1024   Max voltage with resistor divider 10k 2k2
//22.71/1024   Max voltage with resistor divider 10k 2k2 (external ref 4.096)
//Potentially...add 2v drop in bridge (24.71/1024)
//#define VOLTAGECONVERSION 0.0244140625
//#define VOLTAGECONVERSION 27.73/1024
//#define VOLTAGECONVERSION 22.71/1024
//#define VOLTAGECONVERSION 24.71/1024
//#define VOLTAGECONVERSION (24.71/1024)
#define VOLTAGECONVERSION (22.71/1024)

//#define VOLTAGECONVERSION (25.73/1024)
//VOLTAGECONVERSION*CURRENTCONVERSION
//#define POWERCONVERSION 0.00011920928955078125
//#define POWERCONVERSION VOLTAGECONVERSION*CURRENTCONVERSION
#define POWERCONVERSION (VOLTAGECONVERSION*CURRENTCONVERSION)

 //Possible States correspongin to mbus messages (to simplyfy treatment)

#define CMD_SYSSUB_STOP       0x00
#define CMD_SYSSUB_GO         0x01
#define CMD_SYSSUB_OVERLOAD   0x0A

//*************************************************************************************CV****************************************************************
#define CV_CONFIG   510 //Internal CV to store config values

#define CONFIG_booster      0
#define CONFIG_Voltage      1
#define CONFIG_Current      2
#define CONFIG_Power        3
#define CONFIG_MaxCurrent   4
#define CONFIG_Temperature  5
//#define CONFIG_WiFi         7
#define CONFIG_cutout       6
#define CONFIG_CV_Opened    7
#define SHOWNothing         0
#define SHOWCUT             6

//infoShow states (0,1,2,3,4,5,6) corresponding to the bit values ant the SHOWNothing 0 and SHOWCUT

//#define DEFAULT_CONFIG           0b01000101  //Only show Current Not possible to modify CV
#define DEFAULT_CONFIG           0b00000101  //Show Current Not possible to modify CV, not cut_off
#define DEFAULT_MASK_CONFIG      0b11000001  //Mask operation bits (1). Bits to 0 are the ones to select what to show in 7 segments

//***********************************************************************CV_CONFIG2

#define CV_CONFIG2  CV_CONFIG+1 //Internal CV to store config values

#define CONFIG2_Read_Current               0    //By default reading of current on shortcut detectation... not possible to deactivate. This is reserved.
#define CONFIG2_Read_MaxCurrent            1    //By default MaxCurrent only read at setup and if booster is off.This paremeter is reserverd... but not used
#define CONFIG2_Read_Voltage               2
#define CONFIG2_Read_Temperature           3    //Not used,as Temperatrue is read in WiFi module to reduce impact (it takes 3-4 ms the reading... and makes 7sgements flicker)
#define CONFIG2_Read_Power                 4    //Not used, reserved in ase of use of INA266
#define CONFIG2_InternalReading            5    //Use internal AD or INA
#define CONFIG2_Reserved                   6
#define CONFIG2_WiFi                       7

#define SOURCEINTERNAL                     0
#define SOURCEEXTERNAL                     1

#define DEFAULT_CONFIG2           0b10000111 //Use Internal conversor to get current for short...and read all potential values in each loop.

//***********************************************************************CV_T_TEMPERATURE

#define CV_T_TEMPERATURE  CV_CONFIG+2
#define DEFAULT_T_TEMPERATURE 100    //The CV stores multiples of 100ms; 100*100=10s. Take eperature once per second

//***********************************************************************CV_T_MAXCURRENT

#define CV_T_MAXCURRENT  CV_CONFIG+3
#define DEFAULT_T_MAXCURRENT 5    //The CV stores multiples of 100ms; 5*100=0,5s. Take maxcurrent wtice per second

//***********************************************************************CV_T_SHORT_PROTECTION

#define CV_T_SHORT_PROTECTION  CV_CONFIG+4
#define DEFAULT_T_SHORT_PROTECTION 50    //The CV stores multiples of 100ms; 50*100=5s. Every 100ms up to CV_T_SHORT_PROTECTION a checkshort measure will be taken

//***********************************************************************CV_T_WIFI_SERIAL

#define CV_T_WIFI_SERIAL  CV_CONFIG+5
#define DEFAULT_T_WIFI_SERIAL 30    //The CV stores multiples of 100ms; 30*100=3s. Send new measures to wifi module every 3 seconds

#define CV_V_CALIBRE_1  CV_CONFIG+6
#define DEFAULT_V_CALIBRE_1 0    
#define CV_V_CALIBRE_2  CV_CONFIG+7
#define DEFAULT_V_CALIBRE_2 0      //By devault V_Calibre=0*0=0mV    
