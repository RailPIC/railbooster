;*******************************************************************************
;                                                                              *
;    Microchip licenses this software to you solely for use with Microchip     *
;    products. The software is owned by Microchip and/or its licensors, and is *
;    protected under applicable copyright laws.  All rights reserved.          *
;                                                                              *
;    This software and any accompanying information is for suggestion only.    *
;    It shall not be deemed to modify Microchip?s standard warranty for its    *
;    products.  It is your responsibility to ensure that this software meets   *
;    your requirements.                                                        *
;                                                                              *
;    SOFTWARE IS PROVIDED "AS IS".  MICROCHIP AND ITS LICENSORS EXPRESSLY      *
;    DISCLAIM ANY WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING  *
;    BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS    *
;    FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. IN NO EVENT SHALL          *
;    MICROCHIP OR ITS LICENSORS BE LIABLE FOR ANY INCIDENTAL, SPECIAL,         *
;    INDIRECT OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO     *
;    YOUR EQUIPMENT, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR    *
;    SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY   *
;    DEFENSE THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER      *
;    SIMILAR COSTS.                                                            *
;                                                                              *
;    To the fullest extend allowed by law, Microchip and its licensors         *
;    liability shall not exceed the amount of fee, if any, that you have paid  *
;    directly to Microchip to use this software.                               *
;                                                                              *
;    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF    *
;    THESE TERMS.                                                              *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Filename:                                                                 *
;    Date:                                                                     *
;    File Version:                                                             *
;    Author:                                                                   *
;    Company:                                                                  *
;    Description:                                                              *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Notes: In the MPLAB X Help, refer to the MPASM Assembler documentation    *
;    for information on assembly instructions.                                 *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Known Issues: This template is designed for relocatable code.  As such,   *
;    build errors such as "Directive only allowed when generating an object    *
;    file" will result when the 'Build in Absolute Mode' checkbox is selected  *
;    in the project properties.  Designing code in absolute mode is            *
;    antiquated - use relocatable mode.                                        *
;                                                                              *
;*******************************************************************************
;                                                                              *
;    Revision History:                                                         *
;                                                                              *
;*******************************************************************************



;*******************************************************************************
; Processor Inclusion
;
; TODO Step #1 Open the task list under Window > Tasks.  Include your
; device .inc file - e.g. #include <device_name>.inc.  Available
; include files are in C:\Program Files\Microchip\MPLABX\mpasmx
; assuming the default installation path for MPLAB X.  You may manually find
; the appropriate include file for your device here and include it, or
; simply copy the include generated by the configuration bits
; generator (see Step #2).
;
;*******************************************************************************

; TODO INSERT INCLUDE CODE HERE
#ifdef __12F675
#include "p12f675.inc"
#endif
    
#ifdef __12F683
#include "p12f683.inc"
#endif 
    
CONSUMO		EQU 0
OUT1		EQU 1
OUT2		EQU 2
IN		EQU 3
LIMITE		EQU 4

;TO BE VERIFIED

;PIC DOES NOT CONTROL ANYMORE ENABLE... THEREFORE... MEASURES ON CONSUMO AND LIMITE ONLY
;USEFUL TO SET BOTH OUTS LOW (L6203 SINK BOTH CHANNELS)
;THIS IS NOT ENABLE=0 BUT SHOULD HELP CREATING CUTOUT CONDITION v1-v2=0
CUTOUT		EQU 5

TRISIOCOnf	EQU 0x39		;0b00111001 //GP1, GP2 output.. the rest input    
;	GP0 Consumo (AN0)
;	GP1 out1
;	GP2 out2
;	GP3 in      
;	GP4 Limit Potenciometro (AN3)
;	GP5 CUTOUT REQUEST
CMCONConf	EQU 0x07		;0b00000111 //Comparator off
#ifdef __12F675
ANSELConf	EQU b'00011001'	        ;Fosc/8 (a 4Mhez, Tad=2us). AN3 y An0 Analogo
#endif
    
#ifdef __12F683
ANSELConf	EQU b'01011001'	        ;Fosc/16 (a 8Mhez, Tad=2us). AN3 y An0 Analogo
#endif

ADCON0Conf	EQU 0x80		;0b10000000     //Conversion right justified Vdd as Vref
ADCON0AN0on	EQU b'10000001'		;Conversion right justified
ADCON0AN3on	EQU b'10001101'		;Conversion right justified

OPTION_REGConf	EQU 0x87		;0b10000111  //256 prescaler for timer0

;TMR0_30ms	EQU d'118'
;//fosc=4Mhz Tins=1/fosc*4=1us;Tins*prescaler(256)=256us 30ms=117,18*256us.
;//Uso 118; Si se quiere usar interrupci�n comomcontmos hacia adelante... habr�a que usar: 256-118; inicio contador 138

T1CONConf	EQU b'00110001'
;#define T1CONConf   0b00110000  //TIMER1 on, prescaler 8
#ifdef __12F675
;TMR1H_30ms	EQU 0x0F	
TMR1H_30ms	EQU 0x1E	
#endif

#ifdef __12F683
TMR1H_30ms	EQU 0x3B	
#endif

;fosc=4Mhz Tins=1/fosc*4=1us;Tins*prescaler(8)=8us 0x0f00=3840; 3840*8us=30ms
;I will only use TMR1H... if TMR1L is FF, the timer will go until 28ms.
;This simplifies the use of TMR1, looking just to one register
;TMR1L will simply go for ever. I do not use interrupts. This is to detect inactivity only.. I can look at the loop

;#define TMR1_2ms  65286 //fosc=4Mhz Tins=1/fosc*4=1us;Tins*prescaler(8)=8us 2ms=250*8us
;//Overflow 65536-250=; inicio contrador en 65286 cuenta 250 y overflow  //TMR1 16 bits!!!
;#define TMR1_1ms  65411 //1ms=125*8us
;//125 is not a countdown.. but 125 from the start of the clock 65536-125=131

;INTCONConf	EQU 0x88		;0b10001000      //Interrupts avaialble, Peripherial, not available timer0 OFF, and  change GPIO (for GPIO3)
INTCONConf	EQU 0xA8		;0b10101000      //Interrupts avaialble, Peripherial not available, timer0 available, and  change GPIO (for GPIO3)
IOCConf		EQU 0x08		;0b00001000         //Interrupt on change GP3

;//#define PIE1Conf     0b00000001     //TMR1 interrupt 
;#define PIE1Conf     0b00000000     //No TMR1


;TMR0_1ms	EQU d'252'		;4*256us=1ms. 256-5=252; 252(FC), 253(FD), 254(FE), 255(FF)
;TMR0_50ms	EQU d'61'		;195-196*256us=50ms. 256-195=61;

;To be stored in TMR0LIMIT variable		
TMR0_1ms	EQU d'4'		;4*256us=1ms.
TMR0_50ms	EQU d'195'		;195-196*256us=50ms
					;for 12f684 is actually 0,5ms and 25ms
#ifdef __12F675
;To be stored in LIMIT variable		
shortLIMIT	EQU d'20'		;20 ms with short cut off
shortRETURN	EQU d'100'		;100*0,05s= 5s to try to recover from short.
#endif

#ifdef __12F683
;To be stored in LIMIT variable		
shortLIMIT	EQU d'40'		;20 ms with short cut off
shortRETURN	EQU d'200'		;100*0,05s= 5s to try to recover from short.
OSCONConf	EQU b'01110001'		;8Mhz
#endif

;*******************************************************************************
;
; TODO Step #2 - Configuration Word Setup
;
; The 'CONFIG' directive is used to embed the configuration word within the
; .asm file. MPLAB X requires users to embed their configuration words
; into source code.  See the device datasheet for additional information
; on configuration word settings.  Device configuration bits descriptions
; are in C:\Program Files\Microchip\MPLABX\mpasmx\P<device_name>.inc
; (may change depending on your MPLAB X installation directory).
;
; MPLAB X has a feature which generates configuration bits source code.  Go to
; Window > PIC Memory Views > Configuration Bits.  Configure each field as
; needed and select 'Generate Source Code to Output'.  The resulting code which
; appears in the 'Output Window' > 'Config Bits Source' tab may be copied
; below.
;
;*******************************************************************************

; TODO INSERT CONFIG HERE
; CONFIG
#ifdef __12F675
; __config 0xF194
 __CONFIG _FOSC_INTRCIO & _WDTE_OFF & _PWRTE_OFF & _MCLRE_OFF & _BOREN_OFF & _CP_OFF & _CPD_OFF
#endif
    
#ifdef __12F683
; __config 0xF0D4
 __CONFIG _FOSC_INTOSCIO & _WDTE_OFF & _PWRTE_OFF & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _BOREN_OFF & _IESO_OFF & _FCMEN_OFF

#endif 

    
;*******************************************************************************
;
; TODO Step #3 - Variable Definitions
;
; Refer to datasheet for available data memory (RAM) organization assuming
; relocatible code organization (which is an option in project
; properties > mpasm (Global Options)).  Absolute mode generally should
; be used sparingly.
;
; Example of using GPR Uninitialized Data
;
;   GPR_VAR        UDATA
;   MYVAR1         RES        1      ; User variable linker places
;   MYVAR2         RES        1      ; User variable linker places
;   MYVAR3         RES        1      ; User variable linker places
;
;   ; Example of using Access Uninitialized Data Section (when available)
;   ; The variables for the context saving in the device datasheet may need
;   ; memory reserved here.
;   INT_VAR        UDATA_ACS
;   W_TEMP         RES        1      ; w register for context saving (ACCESS)
;   STATUS_TEMP    RES        1      ; status used for context saving
;   BSR_TEMP       RES        1      ; bank select used for ISR context saving
;
;*******************************************************************************
#ifdef __12F675
INT_VAR        UDATA_SHR	    ;https://www.microchip.com/forums/m405057.aspx
#endif
#ifdef __12F683
INT_VAR        UDATA	    ;https://www.microchip.com/forums/m405057.aspx
#endif
W_TEMP         RES        1      ; User variable linker places
STATUS_TEMP    RES        1      ; User variable linker places
LIMITH	       RES	  1
LIMITL	       RES        1
COMSUMH	       RES	  1
CONSUML	       RES        1
SHORTDETECTED  RES	  1	;I could use a byte of flags.. and check jjust one bit
SHORTALERT     RES	  1
LIMIT          RES	  1
TMR0LIMIT      RES	  1
OLDGPIO	       RES	  1
ACTIVEFLAG     RES	  1	;old enable
;REQUEST_M      RES	  1	       
;*******************************************************************************
; Reset Vector
;*******************************************************************************

RES_VECT  CODE    0x0000            ; processor reset vector
    GOTO    START                   ; go to beginning of program

;*******************************************************************************
; TODO Step #4 - Interrupt Service Routines
;
; There are a few different ways to structure interrupt routines in the 8
; bit device families.  On PIC18's the high priority and low priority
; interrupts are located at 0x0008 and 0x0018, respectively.  On PIC16's and
; lower the interrupt is at 0x0004.  Between device families there is subtle
; variation in the both the hardware supporting the ISR (for restoring
; interrupt context) as well as the software used to restore the context
; (without corrupting the STATUS bits).
;
; General formats are shown below in relocatible format.
;
;------------------------------PIC16's and below--------------------------------
;
; ISR       CODE    0x0004           ; interrupt vector location
;
;     <Search the device datasheet for 'context' and copy interrupt
;     context saving code here.  Older devices need context saving code,
;     but newer devices like the 16F#### don't need context saving code.>
;
;     RETFIE
;
;----------------------------------PIC18's--------------------------------------
;
; ISRHV     CODE    0x0008
;     GOTO    HIGH_ISR
; ISRLV     CODE    0x0018
;     GOTO    LOW_ISR
;
; ISRH      CODE                     ; let linker place high ISR routine
; HIGH_ISR
;     <Insert High Priority ISR Here - no SW context saving>
;     RETFIE  FAST
;
; ISRL      CODE                     ; let linker place low ISR routine
; LOW_ISR
;       <Search the device datasheet for 'context' and copy interrupt
;       context saving code here>
;     RETFIE
;
;*******************************************************************************

; TODO INSERT ISR HERE
ISR       CODE    0x0004           ; interrupt vector location
;
;     <Search the device datasheet for 'context' and copy interrupt
;     context saving code here.  Older devices need context saving code,
;     but newer devices like the 16F#### don't need context saving code.>
;

;Sin salvar estado y sin compribar que estoy en banco 0..Podr�a no usar Interrupcion en TMR0... y simplemente controlar el valor en bucle prncipal... pero eso solo me salva una insruccion
; va con se�al 13u
       
;       BTFSS INTCON, GPIF ;1 
;       GOTO TOUT
;       BTFSC GPIO, 3     ;3
;       GOTO IN1;         ;5
;IN2    MOVLW b'00100100'  ;5
;       GOTO ENDIN        ;7
;IN1    MOVLW b'00100010'  ;7
;ENDIN  MOVWF GPIO;       ;8
;       BCF INTCON, GPIF  ;9       
;       CLRF TMR0;        ;10 //1 Deber�a ser antes... puede saltar TNR0 si un tren pulsos rapidos...<20u.. eso no pasar� durante un periodo tan larrgo
;       RETFIE;           ;12
;
;TOUT   BCF GPIO, 5;      ;ENABLE=0
;       BCF INTCON, T0IF;    ;T0IF
;       RETFIE;

;Con salvar contexto me voy hasta15 ciclos...Probado con 20us y funciona (locos)..hasta con 16... por debajo (accesorios.. no va)
       
    MOVWF W_TEMP ;copy W to temp register, could be in either bank   //S1
    SWAPF STATUS,W ;swap status to be saved into W		    //S2
    BCF STATUS,RP0 ;change to bank 0 regardless of current bank	    //S3
    MOVWF STATUS_TEMP ;save status to bank 0 register		    //S4
       
;LO MINIMO sin ToIF... se controlaria en main ni sakvaar
;parece ir a 10us incluso a 7-8us... al meter el salvado de contexto...me ire >13us..que es el limite de los accesorios..aun lwjos de los 20-26 de locos
    
    BTFSC INTCON,T0IF		;2
    GOTO TICK
CHANGE
    BTFSC SHORTDETECTED, 0	;4
    GOTO nOUT
    ;COMF GPIO,W			;5
    COMF GPIO,F			;5
    BSF ACTIVEFLAG,0
    ;IORLW b'00100000'		;6
    ;;;;;;BSF W, 5			;7
    ;MOVWF GPIO			;8
    ;;;;;;CLRF TMR0			;7
    CLRF TMR1H			;9
nOUT
    BCF INTCON, GPIF		;10       
    ;   RETFIE			;12
OUT
    SWAPF STATUS_TEMP,W;swap STATUS_TEMP register into W, sets bank to original state  //S5
    MOVWF STATUS ;move W into STATUS register						//S6
    SWAPF W_TEMP,F ;swap W_TEMP								//S7
    SWAPF W_TEMP,W ;swap W_TEMP into W							//S8
     
    RETFIE

TICK
;OLD CODE INTERRUPT in OVERFLOW every 1ms..
    ;SI SHORTDETECTED UN TIMING.. SI NO.. OTRO TIMING... REINICIAR FLAG.. EINCREMENTAR LA VARIABLE DE PETICIO�N DE MEDIDA
    ;EL PROBLEMA ES QUE QUNQUE QUMENTE LA VARIABLE...YO ESTOY DECREMENTANDO E INCREMENTANDO... NO RESTANDO Y SUMANDO

;    INCFSZ REQUEST_M, F		;4  //IF ff->0.. DEC AND KEEPff
;    GOTO TK			
;    DECF REQUEST_M, F		;5
;TK
;    BTFSC SHORTDETECTED, 0	;7
;    GOTO TKs			;
;    MOVLW shortLIMIT		;8
;    MOVWF LIMIT			;9
;    BCF INTCON, T0IF		;10
;    GOTO OUT			;12
;TKs MOVLW shortRETURN		;9
;    MOVWF LIMIT			;10
;    BCF INTCON, T0IF		;11
;    BTFSC INTCON, GPIF		;13
;    GOTO CHANGE
;    GOTO OUT			;15	    //MUCHOSS TKS.. 13+8=21.. EN LIMITE DE MM; 

;NEW CODE. INTERRUPT on OVERFLOW 256us*256us=65ms without enough measures to ensure not a shortcut
;It actually could have been some measures... but not enough to make TMR0 decrease and not reach the limit
;This should prevent not having measure when ilegal inputs (too fast signals). In this cases, it could happen to never have a measure
    BCF INTCON,T0IF	;I should not clear the flag here...looking for a bug I can not find.
    ;BTFSS GPIO, ENABLE ;iF ALEEADY ZERO..DO NOT EXTEND TIMING TO GO ON
    BTFSS ACTIVEFLAG, 0 ;iF ALEEADY ZERO..DO NOT EXTEND TIMING TO GO ON
    GOTO OUT
    BSF SHORTDETECTED, 0			    
    ;BCF	GPIO, ENABLE	;ENABLE OFF		    
    CALL REPOSE
    MOVLW shortRETURN				    
    MOVWF LIMIT					    
    MOVWF SHORTALERT				    
    MOVLW TMR0_50ms
    MOVWF TMR0LIMIT
    GOTO OUT
;*******************************************************************************
; MAIN PROGRAM
;*******************************************************************************

MAIN_PROG CODE                      ; let linker place main program

START
;INIT
 ;y prueba el control de no actividad... ya veremos si hago el control con tmr1
 
    BCF STATUS,RP0
    BCF INTCON,GIE
    ;CLRF GPIO
    MOVLW b'00000010'		;Compf GPIO requiress GPIO 1,2 are not  init to the same
    MOVWF GPIO
    MOVLW CMCONConf		;0b00000111 //Comparator off
#ifdef __12F675
    MOVWF CMCON
#endif
#ifdef __12F683
    MOVWF CMCON0
    BSF STATUS,RP0
    MOVLW OSCONConf
    MOVWF OSCCON
    BCF STATUS,RP0
#endif  
    MOVLW ADCON0Conf		;0b10000000     //Conversion right justified Vdd as Vref
    MOVWF ADCON0
    BSF STATUS,RP0

    ;MOVLW b'01011001'		;Fosc/16 (a 4Mhez, Tad=4us). AN3 y An0 Analogo
    MOVLW ANSELConf		;Fosc/8 (a 4Mhez, Tad=2us). AN3 y An0 Analogo
    MOVWF ANSEL    

    MOVLW TRISIOCOnf		;0b00011001 //GP0, GP1 GP4 output.. the rest input    
;	GP0 Consumo (AN0)
;	GP1 out1
;	GP2 out2
;	GP3 in      
;	GP4 Limit Potenciometro (AN3)
;	GP5 enable
    MOVWF TRISIO    
    MOVLW OPTION_REGConf	;0b10000111  //256 prescaler for timer0
    MOVWF OPTION_REG    
    CLRF PIE1			;NO TMR1
    MOVLW IOCConf		;GP3 change
    MOVWF IOC			;0b00001000         //Interrupt on change GP3
    BCF STATUS,RP0
    CALL MLIMIT
    CLRF SHORTALERT
    CLRF SHORTDETECTED
    MOVLW shortLIMIT
    MOVWF LIMIT
    MOVLW TMR0_1ms
    MOVWF TMR0LIMIT
    
;START TMR1    
    CLRF TMR1L
    CLRF TMR1H
    MOVLW T1CONConf
    MOVWF T1CON
;RESET TMR0
    CLRF TMR0
;START INTERRUPT    
    MOVLW INTCONConf		;0b10001000      //Interrupts avaialble Peripherial not available timer0 OFF, and  change GPIO (for GPIO3)
    MOVWF INTCON
    
;Start Timeout
;    CLRF TMR0
;    BSF INTCON,T0IE
    
; TODO Step #5 - Insert Your Program Here

LOOP
    ;MOVLW TMR0_30ms
    ;SUBWF TMR0,W
    MOVLW TMR1H_30ms		;L1
    SUBWF TMR1H,W		;L2
    SKPNC			;L4
    CALL TIMEOUT
    MOVF TMR0LIMIT, W		;L5
    SUBWF TMR0,W		;sI GUARDO DIRECTAMENTE PUEDO IR A VALORES NEGATIVOS
    ;SUBWF TMR0,F		;L6
    SKPNC			;L8
;    MOVF REQUEST_M,F
 ;   SKPZ			    ;IF NOT ZERO... CALL MCONSUM
    CALL MCONSUM		;L10
    ;BTFSS GPIO, ENABLE		;L10-L12
    BTFSS ACTIVEFLAG,0		;L10-L12
    CALL MLIMIT			;L12-L14    ; IF NOT ENABLE, KEEP MEASUREING THE LIMIT.. IF ENABLE ON... NO WAY TO CHANGE LIMIT

    ;SET ACTIVE OR NOT...CHECK STATUS CUTOUT
    BTFSC GPIO, CUTOUT		;;set MEANS enable ON.. CUTOFF_OFF; CLEAR...enABLE OFFm,CUTT OFF ON
    GOTO ACTIVE
    CALL REPOSE
    GOTO LOOP                   ;L16-L18     ;loop forever
    ;GOTO $                          ; loop forever
ACTIVE
    BTFSC ACTIVEFLAG,0
    GOTO LOOP  ;IF IT IS ACTIVE ALREADY...NOTHING ELSE TODO
    MOVF OLDGPIO, W
    MOVWF GPIO
    ;CLRF   GPIO		;BOTH OUTS TO ZERO
    BSF	ACTIVEFLAG, 0	;ENABLE OFF
    GOTO LOOP
    
TIMEOUT
    ;BCF GPIO,ENABLE
    CALL REPOSE
    ;CLRF TMR0
    CLRF TMR1H
    CLRF TMR1L
    RETURN

MLIMIT
    MOVLW ADCON0AN3on	
    MOVWF ADCON0
    CALL DELAY		    ;Apparently it is also necessary to wait
    BSF ADCON0, GO
WT  BTFSC ADCON0, GO
    GOTO WT
    MOVF ADRESH, W
    MOVWF LIMITH
    BSF STATUS,RP0
    MOVF ADRESL,W
    BCF STATUS,RP0
    MOVWF LIMITL
    RETURN

MCONSUM
;    DECF REQUEST_M,F				    ;1
    ;DECREASED ALREADY BY TMR0LIMIT
    MOVF TMR0LIMIT, W				    ;+1
    SUBWF TMR0,F				    ;+2
    MOVLW ADCON0AN0on				    ;1
    MOVWF ADCON0				    ;2
    CALL DELAY					    ;tHIS INVOLVES 4US EXTRA
    BSF ADCON0, GO				    ;3
WT2 BTFSC ADCON0, GO				    ;5	
    GOTO WT2						    ;fULL WAIT SHOULD BE 11tAD=11*2US=22US

    MOVF ADRESH, W				    ;6
    SUBWF LIMITH,W				    ;7
    SKPZ					    ;9
    GOTO RESULT16				    ;..11
    BSF STATUS,RP0				    ;10
    MOVF ADRESL,W				    ;11
    BCF STATUS,RP0				    ;12
    SUBWF LIMITL,W				    ;13
RESULT16
    SKPC					    ;15..13
    GOTO SHORT					    ;17
;    MOVF ADRESH, W				    ;6
;    SUBWF LIMITH,W				    ;7
;    SKPC					    ;9
;    GOTO SHORT					    ;iFaddresh>limith
;    SKPNZ
;    GOTO NOSHORT
;    BSF STATUS,RP0				    ;10
;    MOVF ADRESL,W				    ;11
;    BCF STATUS,RP0				    ;12
;    SUBWF LIMITL,W				    ;13
;    SKPC					    ;15
;    GOTO SHORT					    ;**17

NOSHORT
    MOVF SHORTALERT ,F				    ;16
    SKPZ					    ;18
    GOTO SHL					    ;--20
    BCF SHORTDETECTED, 0			    ;19
    MOVLW shortLIMIT				    ;20
    MOVWF LIMIT					    ;21
   ; MOVWF SHORTALERT				    ;
    MOVLW TMR0_1ms				    ;22
    MOVWF TMR0LIMIT				    ;23
    RETURN					    ;25
SHL DECF SHORTALERT,F				    ;--21
    RETURN					    ;--23

SHORT						    ; (caso peor)
    ;MOVLW shortLIMIT				    ;
    MOVF LIMIT,W				    ;**18
    SUBWF SHORTALERT,W				    ;**19
    SKPC	    ;IF C=1 shortALERT>=SHORTLIMIT  ;**21
    GOTO SHM					    ;**-23
    BSF SHORTDETECTED, 0			    ;**22
    ;BCF	GPIO, ENABLE	;ENABLE OFF		    ;**23
    CALL REPOSE
    ;make limit for alert increase... make tmr0 advanc slower is made at interrupt
    MOVLW shortRETURN				    ;**24
    MOVWF LIMIT					    ;**25
    MOVWF SHORTALERT				    ;**26
    MOVLW TMR0_50ms				    ;**27
    MOVWF TMR0LIMIT				    ;**28
    RETURN					    ;**30
SHM INCF SHORTALERT,F				    ;**-24
    RETURN					    ;**-26
    ;hAY QUE SUMAR +2
    
    
DELAY
    #ifdef __12F683
    NOP
    NOP
    NOP
    NOP			;4=2us + 4=2us; total 8us
    #endif
    RETURN		;D4; at this point 4 cycles (4us in 12f675)

REPOSE
    MOVF GPIO, W
    MOVWF OLDGPIO
    CLRF   GPIO		;BOTH OUTS TO ZERO
    BCF	ACTIVEFLAG, 0	;ENABLE OFF
    END