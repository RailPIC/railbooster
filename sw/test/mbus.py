#MBUS TESTING SCRIPT FOR MULTICAST
# ONLY WORKS IN WINDOWS
# ON USE IT WILL SHOW OF MULTICAST TRAFFIC IN THE NETWORK 221.0.0.1
# IT WILL RESPOND TO PING COMMANDS AND SEND STATUS DATA TO THE MULTICAST GROUP (ID_SYSTEM MESSGAE WITH MEASUREMENTS)
# IT WILL NOT RESPOND TO ANY STATUS REQUESTS (ID_SYS_STAT_DATA)
# BUT PRESSINF 's' WILL SEND STATUS DATA TO THE MULTICAST GROUP (FOR CHANNLES 1 TO 3) CHANNEL 0 HAS BEEN REMOVED AS IT SEEMS NOT TO HAVE AN IMPACT IN ASYNCRONOUS REGISTRATION
# ON THE CONTRARY CHANNEL 0 IS THE ONE REQUESTED FOR SYNCRONOUS REGISTRATION WHEN FIRST PING IS DETECTED BY MASTER.
# THE TEST IS PROVIDED THIS WAY AS I INTENTED TO TEST WHAT HAPPENS IF WE DO NOT REACT AUTOMATICALLY TO STATUS REQUESTS, BUT SEND IT ASYNCORNOUS
# IT SEEMS TO WORK. ACTUALY  ROCRAIL SEEMS TO TAKE UP TO 10 (10 CHANNELS) AYSNCRONOUS. IT ENUMERATE 1 CHANNELS BUT ASK FOR THE CORRECT INDEX (v, i and t)
# PRESSING 'q' WILL EXIT THE PROGRAM
# PRESSING '1' TO '4' WILL SEND A STATUS MESSAGE FOR THE CHANNELS 1 TO 4... BUR STATUS MESSAGES (ID_SYSTEM MESSGAE WITH MEASUREMENTS) ARE NOT TREATED IF NOT REGISTERED FIRST

import socket
import struct
import threading
import sys
import select
import os
import msvcrt
import time
import random

# Configuración
#multicast_group = '224.0.0.250'
multicast_group = '224.0.0.1'
RXPort = 15735  # Puerto a escuchar
TXPort = RXPort - 1  # Puerto de envío
#INTERFACE = '0.0.0.0'  # Usar la interfaz predeterminada
INTERFACE = '192.168.2.199'  # Usar la interfaz predeterminada
UIDs = "BO02"


# Definir constantes
BIT_RESPONSE = 0x01
ID_SYSTEM = 0x00
CMD_SYSSUB_STOP = 0x00
CMD_SYSSUB_GO = 0x01
CMD_SYSSUB_STATUS = 0x0B
CMD_SYSSUB_OVERLOAD = 0x0A
CMD_SYSSUB_CLOCK = 0x20
CAN_ID_PING = 0x30
ID_SYS_STAT_DATA = 0x3A
NOHASH = 0

VERSIONM = 1
VERSIONL = 0

zentralUID = [UIDs[0], UIDs[1], UIDs[2], UIDs[3]]
storedHash = [0, 0]
storedHash[0] = ord(zentralUID[3]) ^ ord(zentralUID[1])
storedHash[1] = ord(zentralUID[2]) ^ ord(zentralUID[0])
storedHash[0] |= 0x03  # 0bxxxxxx11
storedHash[1] &= 0x7F  # 0b0xxxxxxx
UID = (ord(zentralUID[0]) << 24) + (ord(zentralUID[1]) << 16) + (ord(zentralUID[2]) << 8) + ord(zentralUID[3])


# Crear el socket UDP
#sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
sock.bind((INTERFACE, RXPort)) # Double parenthese make it a tuple passed as a single argument
sock.settimeout(10) #This prevents an endless time sitting with no data

# Crear el socket UDP para enviar respuestas
sock_tx = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock_tx.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)
sock_tx.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)

#bresult = sock.bind((INTERFACE, RXPort)) # Double parenthese make it a tuple passed as a single argument
# Enlace al puerto UDP
#sock.bind((INTERFACE, RXPort))
#sock.settimeout(10) #This prevents an endless time sitting with no data

# Unirse al grupo multicast (compatibilidad con Windows)
#mreq = struct.pack('4s4s', socket.inet_aton(multicast_group), socket.inet_aton(INTERFACE))
#sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(INTERFACE))
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_group) + socket.inet_aton(INTERFACE))

sock_tx.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(INTERFACE))
sock_tx.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_group) + socket.inet_aton(INTERFACE))

print(f"Escuchando en el grupo multicast {multicast_group}:{RXPort}...")

# Variable de control para salir del bucle
running = True

# Función para leer una tecla sin bloquear
def get_key():
    if msvcrt.kbhit():
        #print("Key pressed"+ msvcrt.getch().decode('utf-8'))
        return msvcrt.getch().decode('utf-8')  # Devuelve la tecla presionada
    return None

# Hilo para detectar la tecla de salida
# def exit_listener():
#     global running
#     input("Presiona ENTER para salir...\n")
#     running = False
#     sock.close()
#     sock_tx.close()

# Hilo para detectar teclas 's'
def key_listener():
     global running
     while running:
        key = get_key()
        if key == 's':  # Si presionas 's'
            print("Enviando mensaje SYS_STAT_DATA")
            #for i in range(4):
            for i in range(1,4):    #Not sending chanel 0. Still working
                sendStatusConfig(i)
                time.sleep(0.5)

            #sendStatusConfig(1)
        if key == 'q':  # Si presionas 's'
            print("Exit")
            running = False

        if key == '1':  # Si presionas '1'
            sysCMD_TEST(1)
        if key == '2':  # Si presionas '2'
            sysCMD_TEST(2)
        if key == '3':  # Si presionas '3'
            sysCMD_TEST(3)
        if key == '4':  # Si presionas '4'
            sysCMD_TEST(4)
        

# Iniciar el hilo de escucha para salida
# thread = threading.Thread(target=exit_listener, daemon=True)
# thread.start()

thread_key = threading.Thread(target=key_listener, daemon=True)
thread_key.start()

def message4me(msg):
    destination = (msg[5] << 24) + (msg[6] << 16) + (msg[7] << 8) + msg[8]
    # print(f"destination {destination}")
    return destination == UID or destination == 0

def parseMgr(data):
    # Obtenemos el segundo byte del mensaje (msg[1])
    msg_type = data[1] if len(data) > 1 else None
    
    # Clasificar el mensaje dependiendo del tipo
    if msg_type == ID_SYSTEM:
        #print("ID_SYSTEM")
        sysCMD(data)
    elif msg_type == CAN_ID_PING:
        process_ping(data)
        #print("CAN_ID_PING")
    elif msg_type == ID_SYS_STAT_DATA:
        process_sys_stat_data(data)
        #print("ID_SYS_STAT_DATA")
    else:
        print(f"COMMAND not considered: {msg_type:02X}")

def sysCMD(msg):
    #print("Processing ID_SYSTEM command with data:", data)
    print("Processing ID_SYSTEM")
    resp_length = 0

    # Crear el array de datos (BUFFSIZE debe ser definido, por ejemplo 20)
    BUFFSIZE = 20  # Puedes ajustarlo según sea necesario
    data = [0] * BUFFSIZE

    # Copiar los primeros 4 elementos de 'zentralUID' en 'data'
    for i in range(4):
        data[i] = ord(zentralUID[i])

    # Asignar los valores de msg[9], msg[10], msg[11] y msg[12] en 'data'
    data[4] = msg[9]
    data[5] = msg[10]  # Este podría no ser necesario para algunos comandos
    data[6] = msg[11]
    data[7] = msg[12]

    if msg[9] == CMD_SYSSUB_STATUS:
        resp_length = 8
        
        # switch (msg[10])
        if msg[10] == 1:  # Current. Según los límites fijos en StatusConfig 5AMP=1024
            #data[6] = rawCurrent[1]
            #data[7] = rawCurrent[0]
            # Alternativamente, puedes usar valores aleatorios
            data[6] = random.randint(0, 2)
            data[7] = random.randint(0, 256)

        elif msg[10] == 2:  # Amn en mA en PT (para CS2) no claros los pasos
            #data[6] = rawCurrent[1]
            #data[7] = rawCurrent[0]
            # Alternativamente, puedes usar valores aleatorios
            data[6] = random.randint(0, 2)
            data[7] = random.randint(0, 256)

        elif msg[10] == 3:  # Volt. Según los límites fijos en StatusConfig 25V=1024
            #data[6] = rawVoltage[1]
            #data[7] = rawVoltage[0]
            # Alternativamente, puedes usar valores aleatorios
            data[6] = random.randint(2, 4)
            data[7] = random.randint(0, 256)

        elif msg[10] == 4:  # T en C Según los límites 0-125
            data[6] = 0x00
            data[7] = random.randint(0, 40) # En Python, puedes asignar el valor directamente si es un número
            # Alternativamente, si `temperature` es un valor de tipo float, puedes convertirlo
            # data[7] = int(temperature)  # Si se necesita un entero

        else:  # Caso por defecto
            data[6] = 0x00
            data[7] = 0x00
        print("Sending STATUS response with chanel:", msg[10])
        sendResponse(0, ID_SYSTEM, BIT_RESPONSE, data, resp_length, NOHASH)

def sysCMD_TEST(channel):
    #print("Processing ID_SYSTEM command with data:", data)
    print("Processing ID_SYSTEM")
    resp_length = 0

    # Crear el array de datos (BUFFSIZE debe ser definido, por ejemplo 20)
    BUFFSIZE = 20  # Puedes ajustarlo según sea necesario
    data = [0] * BUFFSIZE

    # Copiar los primeros 4 elementos de 'zentralUID' en 'data'
    for i in range(4):
        data[i] = ord(zentralUID[i])

    # Asignar los valores de msg[9], msg[10], msg[11] y msg[12] en 'data'
    data[4] = 0x0B
    data[5] = channel
    # data[6] = msg[11]
    # data[7] = msg[12]

    resp_length = 8
    
    # switch (msg[10])
    if channel == 1:  # Current. Según los límites fijos en StatusConfig 5AMP=1024
        #data[6] = rawCurrent[1]
        #data[7] = rawCurrent[0]
        # Alternativamente, puedes usar valores aleatorios
        data[6] = random.randint(0, 2)
        data[7] = random.randint(0, 256)

    elif channel == 2:  # Amn en mA en PT (para CS2) no claros los pasos
        #data[6] = rawCurrent[1]
        #data[7] = rawCurrent[0]
        # Alternativamente, puedes usar valores aleatorios
        data[6] = random.randint(0, 2)
        data[7] = random.randint(0, 256)

    elif channel == 3:  # Volt. Según los límites fijos en StatusConfig 25V=1024
        #data[6] = rawVoltage[1]
        #data[7] = rawVoltage[0]
        # Alternativamente, puedes usar valores aleatorios
        data[6] = random.randint(2, 4)
        data[7] = random.randint(0, 256)

    elif channel == 4:  # T en C Según los límites 0-125
        data[6] = 0x00
        data[7] = random.randint(0, 40) # En Python, puedes asignar el valor directamente si es un número
        # Alternativamente, si `temperature` es un valor de tipo float, puedes convertirlo
        # data[7] = int(temperature)  # Si se necesita un entero

    else:  # Caso por defecto
        data[6] = 0x00
        data[7] = 0x00
    print("Sending NON REQUESTED STATUS response with chanel:", channel)
    sendResponse(0, ID_SYSTEM, BIT_RESPONSE, data, resp_length, NOHASH)

# Función que simula el procesamiento del comando `CAN_ID_PING`
def process_ping(msg):
    # Se realiza alguna acción (aquí solo un ejemplo de simulación)
    data = [0] * 8  # Inicializamos una lista de 8 elementos

    # Copiar los primeros 4 valores de 'zentralUID' a 'data'
    for i in range(4):
        data[i] = ord(zentralUID[i])

    # Asignar los valores de VERSIONM, VERSIONL y los valores constantes
    data[4] = VERSIONM
    data[5] = VERSIONL
    data[6] = 0x00
    data[7] = 0x00  # Valores hardcoded como en el C

# Llamada a sendResponse (ajustada a Python)
    print("Sending PING response with data:", data)
    sendResponse(0, CAN_ID_PING, BIT_RESPONSE, data, 8, NOHASH)
    

# Función que simula el procesamiento del comando `ID_SYS_STAT_DATA`
def process_sys_stat_data(data):
    index = data[9] if len(data) > 9 else None
    print(f"Processing SYS_STAT_DATA with index: {index}")
    # Se pueden agregar más acciones según sea necesario

# Define sendResponse function
def sendResponse(prio, cmd, rsp, data, length, hash_value):
    """
    Sends a response through the TX socket.
    :param prio: Priority byte
    :param cmd: Command byte
    :param rsp: Response byte
    :param data: Data byte array
    :param length: Length of the data
    :param hash_value: Hash value for message
    """
    msg = bytearray(20)  # Assuming BUFFSIZE is 20
    i = 0
    
    # Current Format of the header is working
    msg[0] = (prio << 1)
    msg[1] = cmd
    msg[1] |= rsp
    
    # Hash calculation
    if hash_value == 0:
        msg[2] = storedHash[0]  # Replace with actual storedHash if known
        msg[3] = storedHash[1]  # Replace with actual storedHash if known
    else:
        msg[2] = (hash_value >> 8) & 0xFF  # byte_of(hash, 1)
        msg[3] = hash_value & 0xFF  # byte_of(hash, 0)
    
    msg[4] = length
    for i in range(length):
        msg[5 + i] = data[i]
    
    # Sending the message through a multicast UDP socket
    #with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as udp_socket:
    #    udp_socket.sendto(msg[:length + 5], (multicast_group, TXPort))  # Replace with correct address and port
    sock_tx.sendto(msg[:length + 5], (multicast_group, TXPort))  # Replace with correct address and port    
    #sock_tx.sendto(msg[:length + 5])  # Replace with correct address and port    
    # Optional serial feedback (if relevant in your Python context)
    print("Data OUT: ", end="")
    for i in range(length + 5):
        print(f"{msg[i]:02X}", end=" ")
    print()


# Define sendStatusConfig function
def sendStatusConfig(index):
    """
    Sends the status configuration based on the provided index.
    :param index: The index value to determine which configuration to send
    """
    data = bytearray(8)
    
    for i in range(4):
        data[i] = ord(zentralUID[i])  # Convierte el carácter a su valor entero ASCII
    
    data[4] = index
    
    print(f"MCS2PARSER: Status Config, index: {index}")
    
    if index == 0:
        data[5] = 3  # 3 packets
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH)  # Replace NOHASH with actual value
        
        # Configurations for other data
        data[0] = 0
        data[1] = 1
        data[2] = 0
        data[3] = 0
        data[4] = ord(zentralUID[0])
        data[5] = ord(zentralUID[1])
        data[6] = ord(zentralUID[2])
        data[7] = ord(zentralUID[3])
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1)
        
        data[0] = ord('R')
        data[1] = ord('A')
        data[2] = ord('I')
        data[3] = ord('L')
        data[4] = ord(zentralUID[0])
        data[5] = ord(zentralUID[1])
        data[6] = 0
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2)
        data[0] = ord('B')
        data[1] = ord('O')
        data[2] = ord('O')
        data[3] = ord('S')
        data[4] = ord('T')
        data[5] = ord(zentralUID[2])
        data[6] = ord(zentralUID[3])
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3)
    
    elif index == 1:
        data[5] = 5  # 5 packets
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH)
        
        data[0] = 1
        data[1] = 0xFD
        data[2] = 0x30
        data[3] = 0xF0
        data[4] = 0xE0
        data[5] = 0xC0
        data[6] = 0
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1)
        
        data[0] = 0x04
        data[1] = 0x00
        data[2] = 0x06
        data[3] = 0x00
        data[4] = 0x07
        data[5] = 0x68
        data[6] = 0x03
        data[7] = 0xFF
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2)
        
        data[0] = ord('T')
        data[1] = ord('R')
        data[2] = ord('A')
        data[3] = ord('C')
        data[4] = ord('K')
        data[5] = 0
        data[6] = ord('0')
        data[7] = ord('.')
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3)
        
        data[0] = ord('0')
        data[1] = ord('0')
        data[2] = ord('0')
        data[3] = 0
        data[4] = ord('4')
        data[5] = ord('.')
        data[6] = ord('0')
        data[7] = ord('9')
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 4)
        
        data[0] = ord('6')
        data[1] = 0
        data[2] = ord('A')
        data[3] = 0
        data[4] = 0
        data[5] = 0
        data[6] = 0
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 5)
    
    elif index == 2:
        data[5] = 5  # 5 packets
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH)
        
        data[0] = 3
        data[1] = 0xFD
        data[2] = 0xC0
        data[3] = 0x0C
        data[4] = 0x30
        data[5] = 0xC0
        data[6] = 0
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1)
        
        data[0] = 0x02
        data[1] = 0x66
        data[2] = 0x03
        data[3] = 0x0A
        data[4] = 0x03
        data[5] = 0x85
        data[6] = 0x03
        data[7] = 0xFF
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2)
        
        data[0] = ord('V')
        data[1] = ord('O')
        data[2] = ord('L')
        data[3] = ord('T')
        data[4] = 0
        data[5] = ord('0')
        data[6] = ord('.')
        data[7] = ord('0')
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3)
        
        data[0] = ord('0')
        data[1] = 0
        data[2] = ord('2')
        data[3] = ord('4')
        data[4] = ord('.')
        data[5] = ord('7')
        data[6] = ord('1')
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 4)
        
        data[0] = ord('V')
        data[1] = ord('o')
        data[2] = ord('l')
        data[3] = ord('t')
        data[4] = 0
        data[5] = 0
        data[6] = 0
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 5)
    elif index == 3:
            # data[5] = 4  # 4 packets
        data[5] = 5  # 4 packets
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 6, NOHASH)
        
        # 04 00 0C 08 F0 C0 00 00 canal 4, potencia 4; 4 colores, Nullpunkt
        data[0] = 4
        data[1] = 0
        data[2] = 0x0C
        data[3] = 0x08
        data[4] = 0xF0
        data[5] = 0xC0
        data[6] = 0
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 1)
        
        # 00 6B 00 A4 00 CD 00 DB Límites para los colores
        # data[0] = 0x00; data[1] = 0x6B; data[2] = 0x00; data[3] = 0xA4; data[4] = 0x00; data[5] = 0xCD; data[6] = 0x00; data[7] = 0xDB;
        data[0] = 0x00
        data[1] = 0x1e
        data[2] = 0x00
        data[3] = 0x32
        data[4] = 0x00
        data[5] = 0x64
        data[6] = 0x00
        data[7] = 0x7D  # 7D=125
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 2)
        
        # 54 45 4D 50 00 30 2E 30 SString Messwertbezeichnung \0 Init range \0.
        data[0] = ord('T')  # 'T'
        data[1] = ord('E')  # 'E'
        data[2] = ord('M')  # 'M'
        data[3] = ord('P')  # 'P'
        data[4] = 0
        data[5] = ord('0')  # '0'
        data[6] = ord('.')  # '.'
        data[7] = ord('0')  # '0'
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 3)
        
        # 00 38 30 2E 30 00 43 00 Init range\0+End range +uNIT. To my knowledge it is not fully correct. 
        # As Protocol says number of decimals and potencia must be the same
        # data[0] = 0; data[1] = '8'; data[2] = '0'; data[3] = '.'; data[4] = '0'; data[5] = 0; data[6] = 'C'; data[7] = 0;
        data[0] = 0
        data[1] = ord('1')  # '1'
        data[2] = ord('2')  # '2'
        data[3] = ord('5')  # '5'
        data[4] = ord('.')  # '.'
        data[5] = ord('0')  # '0'
        data[6] = 0
        data[7] = ord('C')  # 'C'
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 4)
        
        data[0] = 0
        data[1] = 0
        data[2] = 0
        data[3] = 0
        data[4] = 0
        data[5] = 0
        data[6] = 0
        data[7] = 0
        sendResponse(0, ID_SYS_STAT_DATA, BIT_RESPONSE, data, 8, 5)

    else:
        print("MCS2PARSER: Status Config, index not defined")
    

try:
    while running:
        #key = get_key()
        #if key == 's':  # Si presionas 's'
        #    print("**********************************************************Enviando mensaje")
        # Recibir exactamente 10 bytes
        data, address = sock.recvfrom(100)
        # Mostrar los datos en formato hexadecimal
        # print("Datos recibidos de:", address)
        # print("Contenido recibido en hexadecimal:")
        # print(" ".join(f"{byte:02X}" for byte in data))
        print(f"Datos recibidos de: {address} " + " ".join(f"{byte:02X}" for byte in data))
        #parseMgr(data)
        if message4me(data):
            parseMgr(data)

except OSError:
    print("\nSocket cerrado, terminando programa...")
finally:
    sock.close()
    sock_tx.close()
